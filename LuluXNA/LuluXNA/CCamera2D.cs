﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

//Code teilweise von verschiedenen Tutorials aus:
//http://www.david-gouveia.com/2d-camera-with-parallax-scrolling-in-xna/ & http://www.david-amador.com/2009/10/xna-camera-2d-with-zoom-and-rotation/

namespace LuluXNA
{
    class CCamera2D
    {
        protected float          _zoom; // Camera Zoom
        public Vector2          _pos; // Camera Position
        protected float         _rotation; // Camera Rotation
        protected Viewport _view;
        protected Vector2 _origin;

        public CCamera2D(Viewport newView)
        {
            _view = newView;
            _zoom = 1.0f;
            _rotation = 0.0f;
            _pos = Vector2.Zero;
            _origin = new Vector2(_view.Width / 2.0f, _view.Height / 2.0f);
        }

        public void Update(CCamera2D cam, CPlayerdata Player){
            float X = Player.msmainChar.Position.X + Player.msmainChar.Sprite.Width / 2 - _view.Width / 2;
                cam.Pos = new Vector2(X, 0);
        }

        public Vector2 WorldToScreen(Vector2 worldPosition, Vector2 parallax)
        {
            return Vector2.Transform(worldPosition, GetViewMatrix(parallax));
        }

        public Vector2 ScreenToWorld(Vector2 screenPosition, Vector2 parallax)
        {
            return Vector2.Transform(screenPosition, Matrix.Invert(GetViewMatrix(parallax)));
        }

        // Sets and gets zoom
        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; if (_zoom < 0.1f) _zoom = 0.1f; } // Negative zoom will flip image
        }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        // Auxiliary function to move the camera
        public void Move(Vector2 amount)
        {
            Pos += amount;
        }
        // Get set position
        public Vector2 Pos
        {
            get { return _pos; }
            set {              //_pos = value;
                _pos = value; 
 
                // If there's a limit set and the camera is not transformed clamp position to limits
                if(Limits != null && Zoom == 1.0f && Rotation == 0.0f)
                {
                    _pos.X = MathHelper.Clamp(_pos.X, Limits.Value.X, Limits.Value.X + Limits.Value.Width - _view.Width);
                    _pos.Y = MathHelper.Clamp(_pos.Y, Limits.Value.Y, Limits.Value.Y + Limits.Value.Height - _view.Height);
                } 
            }       
        }

        public Matrix GetViewMatrix(Vector2 parallax)
        {
            // To add parallax, simply multiply it by the position
            return Matrix.CreateTranslation(new Vector3(-_pos * parallax, 0.0f)) *
                   Matrix.CreateTranslation(new Vector3(-_origin, 0.0f)) *
                   Matrix.CreateRotationZ(Rotation) *
                   Matrix.CreateScale(Zoom, Zoom, 1) *
                   Matrix.CreateTranslation(new Vector3(_origin, 0.0f));
        }

        public Rectangle? Limits
        {
            get { return _limits; }
            set
            {
                if (value != null)
                {
                    // Assign limit but make sure it's always bigger than the viewport
                    _limits = new Rectangle
                    {
                        X = value.Value.X,
                        Y = value.Value.Y,
                        Width = System.Math.Max(_view.Width, value.Value.Width),
                        Height = System.Math.Max(_view.Height, value.Value.Height)
                    };

                    // Validate camera position with new limit
                    Pos = Pos;
                }
                else
                {
                    _limits = null;
                }
            }
        }

        private Rectangle? _limits;
    }    
}
