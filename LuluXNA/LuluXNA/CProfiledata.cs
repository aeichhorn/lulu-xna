﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Xml;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LuluXNA
{
    class CProfiledata
    {
        public string[] m_sName = new string[3];
        public bool[] m_bCat = new bool[3];
        public bool[] m_bSound = new bool[3];
        public bool[] m_bFullscreen = new bool[3];
        public int currentProfile = 0;
        public Texture2D[] Profilepic = new Texture2D[8];
        XmlTextWriter writer;
        XmlReader reader;
        private bool error = false;
        private bool error1 = false;
        CCustomException ex = new CCustomException("This Letter is not available right now! Please choose another letter!");
        CCustomException ex1 = new CCustomException("An Error occured! UnauthorizedAccessException or NullReferenceException in Load or Save Profile!");
        private float _elapsedSeconds;
        private double time;

        public CProfiledata()
        {
            //loadProfiles();
            for (int i = 0; i < 3; i++ )
            {
                m_bCat[i] = false;
                m_bSound[i] = true;
                m_bFullscreen[i] = false;
                m_sName[i] = "Lulu";
            }
        }

        public void LoadProfileContent(ContentManager myContent)
        {
            Profilepic[0] = myContent.Load<Texture2D>("Menu/Profilesbg");
            Profilepic[1] = myContent.Load<Texture2D>("Menu/tick");
            Profilepic[2] = myContent.Load<Texture2D>("Menu/delProfile");
            Profilepic[3] = myContent.Load<Texture2D>("Menu/savexProfile");
            Profilepic[4] = myContent.Load<Texture2D>("Menu/profile1");
            Profilepic[5] = myContent.Load<Texture2D>("Menu/profile2");
            Profilepic[6] = myContent.Load<Texture2D>("Menu/profile3");
            Profilepic[7] = myContent.Load<Texture2D>("Menu/ticksmall");
        }

        #region methods
        public int UpdateProfileMenu(CMenu MMenu, CControls Controller, SpriteBatch spriteBatch, int GState, CLevel1 Lv1, ref CPlayerdata Player, ref CLevel2 Lv2, ref CLevel0 Lv0)
        {
            MMenu.MenuState = Controller.MenuControl(MMenu.MenuState, 8);
            GState = MMenu.ChooseMenu(Controller, spriteBatch, GState);
            if ((Controller.kboldState.IsKeyUp(Keys.Enter) && Controller.keyboardState.IsKeyDown(Keys.Enter)) ||
               (Controller.gamePadState.Buttons.A == ButtonState.Pressed && Controller.gpoldState.Buttons.A == ButtonState.Released) ||
               (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(150, 171, 250, 230)) ||
               (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(140, 218, 300, 270)))
            {
                if (MMenu.MenuState == 7)
                {
                    saveProfile();
                }
                else if (MMenu.MenuState == 6)
                {
                    delProfile(ref Lv1, ref Lv2, ref Player, ref Lv1.MymsgBox, ref Controller, ref Lv0);
                    saveProfile();
                }
                if (MMenu.MenuState > 4)
                    MMenu.MenuState = 0;

                GState = MMenu.MenuChoice;
            }
            if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(300, 75, 360, 175)))
            {
                m_bCat[currentProfile] = false;
            }
            else if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(410, 75, 460, 175)))
            {
                m_bCat[currentProfile] = true;
            }
            else if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(60, 360, 120, 420)))
            {
                currentProfile = 0;
            }
            else if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(170, 360, 230, 420)))
            {
                currentProfile = 1;
            }
            else if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(270, 360, 330, 420)))
            {
                currentProfile = 2;
            }

            m_sName[currentProfile] = Controller.KeyboradInput(m_sName[currentProfile]);

            return GState;
        }

        public void loadProfiles()
        {
            try
            {
                reader = XmlReader.Create("Content/Data/Profiles.xml");
                while (reader.Read())
                {
                    for (int profileNr = 0; profileNr < 3; profileNr++)
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "m_sName")
                            m_sName[profileNr] = reader.ReadElementContentAsString();
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "m_bCat")
                            m_bCat[profileNr] = reader.ReadElementContentAsBoolean();
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "m_bSound")
                            m_bSound[profileNr] = reader.ReadElementContentAsBoolean();
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "m_bFullscreen")
                            m_bFullscreen[profileNr] = reader.ReadElementContentAsBoolean();
                    }
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "currentProfile")
                        currentProfile = reader.ReadElementContentAsInt();
                }
                reader.Close();
            }
            catch (System.Xml.XmlException)
            {
                reader.Close();
            }
            catch (System.NullReferenceException)
            {
                error1 = true;
            }
            catch (System.UnauthorizedAccessException)
            {
                error1 = true;
            }
        }

        public void saveProfile()
        {
            try
            {
                writer = new XmlTextWriter("Content/Data/Profiles.xml", null);
                writer.WriteStartDocument();
                writer.WriteStartElement("mainnode");
                for (int profileNr = 0; profileNr < 3; profileNr++)
                {
                    writer.WriteStartElement("m_sName");
                    writer.WriteString(m_sName[profileNr]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("m_bCat");
                    writer.WriteValue(m_bCat[profileNr]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("m_bSound");
                    writer.WriteValue(m_bSound[profileNr]);
                    writer.WriteEndElement();
                    writer.WriteStartElement("m_bFullscreen");
                    writer.WriteValue(m_bFullscreen[profileNr]);
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("currentProfile");
                writer.WriteValue(currentProfile);
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }
            catch (System.Xml.XmlException)
            {
                writer.Close();
            }
            catch (System.NullReferenceException)
            {
                error1 = true;
            }
            catch (System.UnauthorizedAccessException)
            {
                error1 = true;
            }
        }

        public void delProfile(ref CLevel1 Lv1, ref CLevel2 Lv2, ref CPlayerdata Player, ref CMsBox MBox, ref CControls Controller, ref CLevel0 Lv0)
        {
            m_bCat[currentProfile] = false;
            m_bFullscreen[currentProfile] = false;
            m_bSound[currentProfile] = true;
            m_sName[currentProfile] = "Lulu";

            Lv1.Levelobj[8].Pos = new Vector2(672, 560);
            Player.isAlive = true;
            Player.msmainChar.Sprite.CurrentAnimation = "idle";
            Player.velocity = Vector2.Zero;
            Player.gravity = false;
            Player.mDirection = Vector2.Zero;
            MBox.Count = 0;
            Lv1.Raven.Position = new Vector2(1000, -180);
            Lv1.Raven.Target = new Vector2(1472, 400);
            Lv1.Raven.Sprite.CurrentAnimation = "flying";
            Player.PoldPos = new Vector2(200, 620);
            Player.Lv = 1;
            Player.msmainChar.Position = new Vector2(200, 620);
            for (int i = 0; i < Lv1.msgcount; i++)
            {
                Lv1.didShowMsgB[i] = false;
            }
            Lv1.Speaking = false;
            Lv1.gate = false;
            Controller.PKeyPressed = false;
            Controller.MState = 0;
            Controller.CState = 0;
            Controller.JState = 2;
            Lv1.RavenMsg.show = false;
            Lv1.LState = 0;
            Lv2.save = false;
            Lv2.Levelobj[3].Pos = new Vector2(2315, 377);
            Lv2.Levelobj[0].Pos = new Vector2(2733, -100);
            Lv2.didShowMsgB[0] = false;
            Lv2.MymsgBox.showMsgB = false;
            Lv2.MymsgBox.Count = 0;
            Lv0.fadein = false;
            Lv1.fadeout = false;
            Lv1.fadein = false;
            Lv2.fadein = false;
            Lv2.fadeout = false;
            Lv0.alpha = 0f;
            Lv1.alpha = 1f;
            Lv2.alpha1 = 1f;
            Lv2.alpha2 = 0f;
            Lv2.rocksound = false;
            Lv2.deadfadein = false;
            Lv2.deadfadeout = false;
            Lv0.Lv0Obj[1].Pos = new Vector2(789, 520);
            Player.saveGame(Lv1.LState, Lv2.LState, 4, (int)Lv1.Levelobj[8].Pos.X, Lv1.MymsgBox.Count, Lv1.Raven, Lv1.msgcount, Lv1.didShowMsgB, Lv1.Speaking, Lv1.gate, ref Lv1.RavenMsg.show,
                ref Lv2.save, ref Lv2.Levelobj, ref Lv2.didShowMsgB[0],ref Lv2.MymsgBox.Count);
            saveProfile();
        }

        public void DrawProfileMenu(SpriteBatch spriteBatch, CPlayerdata Player, CControls Controller, CMenu MMenu, GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(Player.Profilepic[0], new Vector2(0, 0), Color.White);
            MMenu.DMenu(Controller, spriteBatch);

            try
            {
                spriteBatch.DrawString(MMenu.fontbig, Player.m_sName[Player.currentProfile].ToString(), new Vector2(60, 110), Color.White);
            }
            catch (System.ArgumentException)
            {
                error = true;
                Player.m_sName[Player.currentProfile] = Player.m_sName[Player.currentProfile].Substring(0, Player.m_sName[Player.currentProfile].Length - 1);
            }

            if (error == true || error1 == true)
            {
                time = gameTime.ElapsedGameTime.TotalSeconds;
                _elapsedSeconds += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (_elapsedSeconds - time >= 3)
                {
                    error = false;
                    error1 = false;
                    _elapsedSeconds = 0;
                } 
            }

            if (error == true)
            {
                spriteBatch.DrawString(MMenu.font, ex.errormessage, new Vector2(60, 10), Color.Red);
            }
            if (error1 == true)
            {
                spriteBatch.DrawString(MMenu.font, ex1.errormessage, new Vector2(60, 10), Color.Red);
            }

            if (Controller.MouseCollision(150, 171, 250, 230))
            {
                MMenu.MenuState = 6;
            }
            else if (Controller.MouseCollision(140, 218, 300, 270))
            {
                MMenu.MenuState = 7;
            }
            if (MMenu.MenuState == 6)
            {
                spriteBatch.Draw(Player.Profilepic[2], new Vector2(148, 171), Color.White);
                MMenu.MenuChoice = 2;
            }
            else if (MMenu.MenuState == 7)
            {
                spriteBatch.Draw(Player.Profilepic[3], new Vector2(140, 218), Color.White);
                MMenu.MenuChoice = 2;
            }
            if (Player.m_bCat[Player.currentProfile] == false)
            {
                spriteBatch.Draw(Player.Profilepic[1], new Vector2(300, 75), Color.White);
            }
            else
            {
                spriteBatch.Draw(Player.Profilepic[1], new Vector2(410, 75), Color.White);
            }

            spriteBatch.Draw(Player.Profilepic[4], new Vector2(60, 360), Color.White);
            spriteBatch.Draw(Player.Profilepic[5], new Vector2(160, 360), Color.White);
            spriteBatch.Draw(Player.Profilepic[6], new Vector2(270, 360), Color.White);

            switch (Player.currentProfile)
            {
                case 0:
                    spriteBatch.Draw(Player.Profilepic[7], new Vector2(70, 360), Color.White);
                    break;
                case 1:
                    spriteBatch.Draw(Player.Profilepic[7], new Vector2(170, 360), Color.White);
                    break;
                case 2:
                    spriteBatch.Draw(Player.Profilepic[7], new Vector2(270, 360), Color.White);
                    break;
            }
            spriteBatch.End();
        }

        #endregion

    }
}
