﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

//Kollision teilweise aus einem Microsoft Tutorial von der msdn Seite

namespace LuluXNA
{
    class CCollision
    {
        // The color data for the images; used for per pixel collision
        Color[] object1TextureData;
        Color[] object2TextureData;
        public Texture2D cropobj2;

        // For when a collision is detected
        public bool objectHit = false;
        Rectangle object2Rectangle;
        Rectangle object1Rectangle;

        public CCollision()
        {
        }

        public bool Intersects(Rectangle rectA, Rectangle rectB)
        {
            // Returns True if rectA and rectB contain any overlapping points
            return (rectA.Right > rectB.Left && rectA.Left < rectB.Right &&
                    rectA.Bottom > rectB.Top && rectA.Top < rectB.Bottom);
        }

        public bool ColorCollision(CMobileSprites Sprite, Texture2D Object2, Color colColor)
        {
            cropobj2 = Crop(Object2, Sprite.BoundingBox);
            Color[] TextureData = new Color[cropobj2.Width * cropobj2.Height];
            cropobj2.GetData<Color>(TextureData);

            for (int i = 0; i < cropobj2.Width; i++)
                for (int j = 0; j < cropobj2.Height; j++)
                    if (TextureData[i * j] == colColor)
                        return true;

            return false;
        }

        private void ExtractCollisionData(Texture2D object1Texture, Texture2D object2Texture, Vector2 object1Pos, Vector2 object2Pos)
        {
            try
            {
                // Extract collision data
                object1TextureData =
                    new Color[object1Texture.Width * object1Texture.Height];
                object1Texture.GetData(object1TextureData);
                 object2TextureData =
                    new Color[object2Texture.Width * object2Texture.Height];
                object2Texture.GetData(object2TextureData);
               // Get the bounding rectangle of object 2
                object2Rectangle =
                    new Rectangle((int)object2Pos.X, (int)object2Pos.Y,
                    object2Texture.Width, object2Texture.Height);
                // Get the bounding rectangle of object 1 
               object1Rectangle =
                    new Rectangle((int)object1Pos.X, (int)object1Pos.Y,
                    object1Texture.Width, object1Texture.Height);
            }
            catch (System.NullReferenceException)
            {
                return;
            }
        }


        public bool PixelperPixelCollision(CMobileSprites msmainChar, Texture2D object2Texture, Vector2 object1Pos, Vector2 object2Pos)
        {
            CFrameAnimation myFrame = msmainChar.Sprite.CurrentCFrameAnimation;
            Texture2D object1Texture = Crop(msmainChar.Sprite.Texture, myFrame.FrameRectangle);
            ExtractCollisionData(object1Texture, object2Texture, object1Pos, object2Pos);
            // Check collision with object
            if (IntersectPixels(object1Rectangle, object1TextureData,
                                object2Rectangle, object2TextureData))
            {
                objectHit = true;
            }
            else
            {
                objectHit = false;
            }
            return objectHit;
        }

        /// <summary>
        /// Determines if there is overlap of the non-transparent pixels
        /// between two sprites.
        /// </summary>
        /// <param name="rectangleA">Bounding rectangle of the first sprite</param>
        /// <param name="dataA">Pixel data of the first sprite</param>
        /// <param name="rectangleB">Bouding rectangle of the second sprite</param>
        /// <param name="dataB">Pixel data of the second sprite</param>
        /// <returns>True if non-transparent pixels overlap; false otherwise</returns>
        static bool IntersectPixels(Rectangle rectangleA, Color[] dataA,
                                    Rectangle rectangleB, Color[] dataB)
        {
            // Find the bounds of the rectangle intersection
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            // Check every point within the intersection bounds
            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    // Get the color of both pixels at this point
                    Color colorA = dataA[(x - rectangleA.Left) +
                                         (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = dataB[(x - rectangleB.Left) +
                                         (y - rectangleB.Top) * rectangleB.Width];

                    // If both pixels are not completely transparent,
                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        // then an intersection has been found
                        return true;
                    }
                }
            }

            // No intersection found
            return false;
        }

        /// <summary>
        /// Crops the Texture
        /// </summary>
        /// <param name="source">Source Texture</param>
        /// <param name="area">Rectangle where to crop</param>
        /// <returns></returns>
        public static Texture2D Crop(Texture2D source, Rectangle area)
        {
            try
            {
                if (source == null)
                    return null;

                Texture2D cropped = new Texture2D(source.GraphicsDevice, area.Width, area.Height);
                Color[] data = new Color[source.Width * source.Height];
                Color[] cropData = new Color[cropped.Width * cropped.Height];

                source.GetData<Color>(data);

                int index = 0;
                for (int y = area.Y; y < area.Y + area.Height; y++)
                {
                    for (int x = area.X; x < area.X + area.Width; x++)
                    {
                        cropData[index] = data[x + (y * source.Width)];
                        index++;
                    }
                }

                cropped.SetData<Color>(cropData);

                return cropped;
            }
            catch (System.IndexOutOfRangeException)
            {
                return null;
            }
        }

        public static Texture2D Flip(Texture2D source, bool vertical, bool horizontal)
        {
            Texture2D flipped = new Texture2D(source.GraphicsDevice, source.Width, source.Height);
            Color[] data = new Color[source.Width * source.Height];
            Color[] flippedData = new Color[data.Length];

            source.GetData<Color>(data);

            for (int x = 0; x < source.Width; x++)
                for (int y = 0; y < source.Height; y++)
                {
                    int idx = (horizontal ? source.Width - 1 - x : x) + ((vertical ? source.Height - 1 - y : y) * source.Width);
                    flippedData[x + y * source.Width] = data[idx];
                }

            flipped.SetData<Color>(flippedData);

            return flipped;
        }

        const int penetrationMargin = 5;
        public bool IsOnTopOf(Rectangle r1, Rectangle r2)
        {
            return (r1.Bottom >= r2.Top - penetrationMargin && r1.Bottom <= r2.Top + 1 &&
                r1.Right >= r2.Left + 5 && r1.Left <= r2.Right - 5);
        }

        public bool IsOnTheLeft(Rectangle r1, Rectangle r2, ref float mDirection)
        {
            if (mDirection == 1)
            {
                return (r1.Right >= r2.Left + penetrationMargin && r1.Right <= r2.Left + r2.Width / 4 &&
                    r1.Bottom >= r2.Top - 5 && r1.Top <= r2.Bottom);
            }
            else
            {
                return false;
            }
        }

        public bool IsOnTheRight(Rectangle r1, Rectangle r2, ref float mDirection)
        {
            if (mDirection == -1)
            {
                return (r1.Left <= r2.Right - penetrationMargin && r1.Left >= r2.Right - r2.Width / 4 &&
                    r1.Bottom >= r2.Top - 5 && r1.Top <= r2.Bottom);
            }
            else
            {
                return false;
            }
        }

    }
}
