﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LuluXNA
{
    class CGameObject
    {
        public Texture2D Texture;
        public int _passable; 
        public Vector2 Pos;

        /// <summary>
        /// Controls the collision detection and response behavior of a tile.
        /// </summary>
        enum OjbCollision
        {
            /// <summary>
            /// A passable tile is one which does not hinder player motion at all.
            /// </summary>
            Passable = 0,

            /// <summary>
            /// An impassable tile is one which does not allow the player to move through
            /// it at all. It is completely solid.
            /// </summary>
            Impassable = 1,

            /// <summary>
            /// A platform tile is one which behaves like a passable tile except when the
            /// player is above it. A player can jump up through a platform as well as move
            /// past it to the left and right, but can not fall down through the top of it.
            /// </summary>
            Platform = 2,
        }


        public CGameObject()
        {
            Pos = new Vector2(0, 0);
        }

         public int Passable
         {
             get { return _passable; }
             set { _passable = value; }
         }

        /// <summary>
        /// Gets a texture origin at the bottom center of each frame.
        /// </summary>
        public Vector2 Origin
        {
            get { return new Vector2(Texture.Width / 2.0f, Texture.Height); }
        }

        /// <summary>
        /// Gets a rectangle which bounds this player in world space.
        /// </summary>
        public Rectangle BoundingRectangle
        {
            get
            {
                int left = (int)Pos.X;
                int top = (int)Pos.Y;

                return new Rectangle(left, top, Texture.Width, Texture.Height);
            }
        }

    }
}
