﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace LuluXNA
{
    class CMenu
    {
        public Texture2D[] menuNavi = new Texture2D[11];
        public int MenuState = 0;
        public int MenuChoice;
        enum GameState { SplashScreen = 1, Menu, InGameMenu, Playing, Paused, LoadGame, Options, Credits, End, Profile, Lv2, Save, Paused1, Lv0 };
        public Texture2D Creditsbg, splashscreen, m_menuTex;
        public Texture2D[] Optionspic = new Texture2D[2];
        public SpriteFont font, fontbig;
        public Texture2D m_mouseTexture;
        public bool Profileloaded = false;
        public SoundEffect[] effect = new SoundEffect[2];
        public SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[2];

        public int ChooseMenu(CControls Controller, SpriteBatch spriteBatch, int GState)
        {
            //Choose sth. from the menu
            if ((Controller.kboldState.IsKeyUp(Keys.Enter) && Controller.keyboardState.IsKeyDown(Keys.Enter)) ||
                (Controller.gamePadState.Buttons.A == ButtonState.Pressed && Controller.gpoldState.Buttons.A == ButtonState.Released) ||
                (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(715, 118, 815, 170)) ||
                (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(725, 220, 915, 320)) ||
                (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(715, 350, 815, 450)) ||
                (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(715, 465, 915, 520)) ||
                (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(914, 679, 1024, 768)))
            {
                return MenuChoice;
            }

            return GState;
        }

        public void LoadContent(ContentManager myContent)
        {
            menuNavi[0] = myContent.Load<Texture2D>("Menu/Play");
            menuNavi[1] = myContent.Load<Texture2D>("Menu/LoadGame");
            menuNavi[2] = myContent.Load<Texture2D>("Menu/Options");
            menuNavi[3] = myContent.Load<Texture2D>("Menu/Credits");
            menuNavi[4] = myContent.Load<Texture2D>("Menu/End");
            menuNavi[5] = myContent.Load<Texture2D>("Menu/editProfile");
            menuNavi[6] = myContent.Load<Texture2D>("Menu/pro_black");
            menuNavi[7] = myContent.Load<Texture2D>("Menu/pro_grey");
            menuNavi[8] = myContent.Load<Texture2D>("Menu/backbtn");
            menuNavi[9] = myContent.Load<Texture2D>("Menu/Ingame_Menu");
            menuNavi[10] = myContent.Load<Texture2D>("Menu/saveGame");
            Creditsbg = myContent.Load<Texture2D>("Menu/Creditsbg");
            Optionspic[0] = myContent.Load<Texture2D>("Menu/Optionsbg");
            Optionspic[1] = myContent.Load<Texture2D>("Menu/offbutton");
            splashscreen = myContent.Load<Texture2D>("Menu/Splashscreen");
            font = myContent.Load<SpriteFont>("Fonts/Arial");
            fontbig = myContent.Load<SpriteFont>("Fonts/Arialbig");
            m_menuTex = myContent.Load<Texture2D>("Menu/Menubg");
            m_mouseTexture = myContent.Load<Texture2D>("Menu/Mousepointer");
            effect[0] = myContent.Load<SoundEffect>("Audio/Katziwatzi");
            soundEffectInstance[0] = effect[0].CreateInstance();
            soundEffectInstance[0].IsLooped = true;
            effect[1] = myContent.Load<SoundEffect>("Audio/checkpoint");
            soundEffectInstance[1] = effect[1].CreateInstance();
            soundEffectInstance[1].IsLooped = false;
        }

        public int UpdateMenu(CPlayerdata Player, CControls Controller, int GState, SpriteBatch spriteBatch)
        {
                if (Profileloaded == false)
                {
                    Player.loadProfiles();
                    Profileloaded = true;
                }
                MenuState = Controller.MenuControl(MenuState, 7);
                if ((Controller.kboldState.IsKeyUp(Keys.Enter) && Controller.keyboardState.IsKeyDown(Keys.Enter)) ||
                    (Controller.gamePadState.Buttons.A == ButtonState.Pressed && Controller.gpoldState.Buttons.A == ButtonState.Released) ||
                    (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(150, 171, 250, 230)))
                {
                    GState = MenuChoice;
                }
                GState = ChooseMenu(Controller, spriteBatch, GState);
                return GState;
         }

         public int UpdateOptions(CPlayerdata Player, SpriteBatch spriteBatch, CControls Controller, int GState, GraphicsDeviceManager graphics){
                MenuState = Controller.MenuControl(MenuState, 6);
                GState = ChooseMenu(Controller, spriteBatch, GState);
                if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(186, 218, 257, 280)))
                {
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                        Player.m_bSound[Player.currentProfile] = false;
                        if (soundEffectInstance[0].State != SoundState.Stopped)
                            {
                                soundEffectInstance[0].Stop();
                            }
                    }
                    else if (Player.m_bSound[Player.currentProfile] == false)
                    {
                        Player.m_bSound[Player.currentProfile] = true;
                        if (soundEffectInstance[0].State == SoundState.Stopped)
                        {
                            soundEffectInstance[0].Play();
                        }
                    }
                    Player.saveProfile();
                }
                else if ((Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released && Controller.MouseCollision(272, 375, 339, 428)))
                {
                    if (Player.m_bFullscreen[Player.currentProfile] == true)
                    {
                        Player.m_bFullscreen[Player.currentProfile] = false;
                        //Switch to Fullscreen-Mode or Windwow-Mode
                        graphics.ToggleFullScreen();
                        graphics.ApplyChanges();
                    }
                    else if (Player.m_bFullscreen[Player.currentProfile] == false)
                    {
                        Player.m_bFullscreen[Player.currentProfile] = true;
                        //Switch to Fullscreen-Mode or Windwow-Mode
                        graphics.ToggleFullScreen();
                        graphics.ApplyChanges();
                    }
                    Player.saveProfile();
                }
             return GState;
         }
 
        public void DMenu(CControls Controller, SpriteBatch spriteBatch)
        {
#if WINDOWS
            if (Controller.MouseCollision(715, 118, 815, 170))
            {
                MenuState = 0;
            }
            else if (Controller.MouseCollision(725, 220, 915, 320))
            {
                MenuState = 1;
            }
            else if (Controller.MouseCollision(715, 350, 815, 450))
            {
                MenuState = 2;
            }
            else if (Controller.MouseCollision(715, 465, 915, 520))
            {
                MenuState = 3;
            }
            else if (Controller.MouseCollision(715, 575, 915, 620))
            {
                MenuState = 4;
            }
            else if (Controller.MouseCollision(914, 679, 1024, 768))
            {
                MenuState = 5;
            }
#endif
            switch (MenuState)
            {
                case 0: 
                    spriteBatch.Draw(menuNavi[0], new Vector2(715, 120), Color.White);
                    MenuChoice = (int)GameState.Lv0;
                    break;
                case 1:
                    spriteBatch.Draw(menuNavi[1], new Vector2(725, 223), Color.White);
                    MenuChoice = (int)GameState.LoadGame;
                    break;
                case 2:
                    spriteBatch.Draw(menuNavi[2], new Vector2(715, 350), Color.White);
                    MenuChoice = (int)GameState.Options;
                    break;
                case 3:
                    spriteBatch.Draw(menuNavi[3], new Vector2(715, 466), Color.White);
                    MenuChoice = (int)GameState.Credits;
                    break;
                case 4:
                    spriteBatch.Draw(menuNavi[4], new Vector2(715, 577), Color.White);
                    MenuChoice = (int)GameState.End;
                    break;
                case 5:
                    spriteBatch.Draw(menuNavi[8], new Vector2(914, 679), Color.White);
                    MenuChoice = (int)GameState.Menu;
                    break;
                default:
                    break;
            }
        }

        public void DrawSplashScreen(SpriteBatch spriteBatch){
            spriteBatch.Begin();
            spriteBatch.Draw(splashscreen, new Vector2(0, 0), Color.White);
            spriteBatch.DrawString(font, "(Press Enter - CLICK - Gamepad A)", new Vector2(540, 500), Color.White);
            spriteBatch.End();
        }

        public void DrawIngameMenu(SpriteBatch spriteBatch, int MState, CCamera2D cam, CMenu MMenu, CPlayerdata Player, Vector2 parallax)
        {
            spriteBatch.Draw(MMenu.menuNavi[9], cam.Pos, Color.White);
            switch (MState)
            {
                case 1:
                    spriteBatch.Draw(menuNavi[10], cam.ScreenToWorld(new Vector2(485, 230), parallax), Color.White);
                    break;
                case 2:
                    spriteBatch.Draw(menuNavi[4], cam.ScreenToWorld(new Vector2(489, 346), parallax), Color.White);
                    break;
                default:
                    spriteBatch.Draw(menuNavi[8], cam.ScreenToWorld(new Vector2(482, 110), parallax), Color.White);
                    break;
            }
        }

        public void DrawMenu(SpriteBatch spriteBatch, CPlayerdata Player, CControls Controller)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(m_menuTex, new Vector2(0, 0), Color.White);
            spriteBatch.DrawString(fontbig, Player.m_sName[Player.currentProfile].ToString(), new Vector2(60, 110), Color.White);
            //Check Mouseposition and draw Menu
            DMenu(Controller, spriteBatch);
            if (Controller.MouseCollision(150, 171, 250, 230))
            {
                MenuState = 6;
            }
            if (MenuState == 6)
            {
                spriteBatch.Draw(menuNavi[5], new Vector2(150, 171), Color.White);
                MenuChoice = (int)GameState.Profile;
            }
            //Show Profile Picture
            if (Player.m_bCat[Player.currentProfile] == false)
            {
                spriteBatch.Draw(menuNavi[6], new Vector2(280, 75), Color.White);
            }
            else
            {
                spriteBatch.Draw(menuNavi[7], new Vector2(280, 75), Color.White);
            }
            spriteBatch.End();
        }

        public void DrawCredits(SpriteBatch spriteBatch, CControls Controller)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(Creditsbg, new Vector2(0, 0), Color.White);
            DMenu(Controller, spriteBatch);
            spriteBatch.End();
        }

        public void DrawOptions(SpriteBatch spriteBatch, CControls Controller, CPlayerdata Player)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(Optionspic[0], new Vector2(0, 0), Color.White);
            DMenu(Controller, spriteBatch);
            if (Player.m_bSound[Player.currentProfile] == false)
            {
                spriteBatch.Draw(Optionspic[1], new Vector2(206, 218), Color.White);
            }
            if (Player.m_bFullscreen[Player.currentProfile] == false)
            {
                spriteBatch.Draw(Optionspic[1], new Vector2(290, 375), Color.White);
            }
            spriteBatch.End();
        }


    }
}
