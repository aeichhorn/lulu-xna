﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LuluXNA
{
    public class CControls
    {
        public GamePadState gamePadState, gpoldState;
        public KeyboardState keyboardState, kboldState;
        public MouseState mouseState, moldState;
        public bool pauseKeyDown = false;
        public Vector2 m_mousePos;
        Keys lastKey = Keys.None;
        public bool PKeyPressed = false;
        public int MState = 0;
        private float _elapsedSeconds;
        private double startPress;
        public Texture2D black_rect;

        enum CharState { Idle = 0, Leftwalk, Rightwalk, Rightrun, Leftrun, Rightjump, Leftjump, JumpIdle, Speaking };
        enum GameState { SplashScreen = 1, Menu, InGameMenu, Playing, Paused, LoadGame, Options, Credits, End, Profile, Lv2, Save, Paused1};
        enum JumpState { Jumping, Falling, nJumping };
        enum Lv1State  { Start, Loading, Ravenin, Ravenout, Key, GotKey, Gateopen, Playing };
        public int CState = 0;
        public int JState = 2;

        public void LoadContent(ContentManager myContent)
        {
            black_rect = myContent.Load<Texture2D>("Level2/Lv2_e1");
        }

        public bool exitKeyPressed()
        {
            // Check to see whether ESC was pressed on the keyboard or BACK was pressed on the controller.
            if ((kboldState.IsKeyUp(Keys.Q) && keyboardState.IsKeyDown(Keys.Q)) || gamePadState.Buttons.Back == ButtonState.Pressed)
            {
                return true;
            }
            return false;
        }

        public bool endPressed(CControls Controller, int MenuChoice, int Endnr)
        {
            if ((Controller.moldState.LeftButton == ButtonState.Released && Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.MouseCollision(715, 575, 915, 620)) ||
                (MenuChoice == Endnr && (Controller.keyboardState.IsKeyDown(Keys.Enter) ||
                (Controller.gamePadState.Buttons.A == ButtonState.Pressed && Controller.gpoldState.Buttons.A == ButtonState.Released))))
            {
                return true;
            }
            return false;
        }

        public void checkPauseKey()
        {
            if ((kboldState.IsKeyUp(Keys.P) && keyboardState.IsKeyDown(Keys.P)) || (gamePadState.Buttons.Y == ButtonState.Pressed))
            {
                pauseKeyDown = true;
            }
            else if (pauseKeyDown)
            {
                pauseKeyDown = false;
            }
        }

        public bool MouseCollision(long targetx, long targety, long targetx1, long targety1)
        {
            bool missed = (m_mousePos.X > targetx && m_mousePos.X < targetx1 &&
            m_mousePos.Y > targety && m_mousePos.Y < targety1);
            return missed;
        }

        public int MenuControl(int MenuState, int MenuPointNo)
        {
            //Keyboardcontrol & GamePadcontrol in the Menu
            if ((kboldState.IsKeyUp(Keys.Down) && keyboardState.IsKeyDown(Keys.Down)) ||
                (gamePadState.DPad.Down == ButtonState.Pressed && gpoldState.DPad.Down == ButtonState.Released && gamePadState.IsConnected))
            {
                MenuState = (MenuState + 1) % MenuPointNo;
            }
            else if ((kboldState.IsKeyUp(Keys.Up) && keyboardState.IsKeyDown(Keys.Up)) ||
                    (gamePadState.DPad.Up == ButtonState.Pressed && gpoldState.DPad.Up == ButtonState.Released && gamePadState.IsConnected))
            {
                if (MenuState < 1)
                    MenuState = MenuPointNo;
                MenuState -= 1;
            }

            return MenuState;
        }

        public bool BPressed()
        {
             if ((keyboardState.IsKeyDown(Keys.Enter) && kboldState.IsKeyUp(Keys.Enter)) ||
            (gamePadState.Buttons.B == ButtonState.Pressed && gamePadState.IsConnected && gpoldState.Buttons.B == ButtonState.Released))
            {
                return true;
            }
             return false;
        }

        public int GetInput(int GState, GameTime gameTime, bool gravity, int Lv)
        {
            if ((keyboardState.IsKeyDown(Keys.P) && kboldState.IsKeyUp(Keys.P)) ||
                (gamePadState.Buttons.Start == ButtonState.Pressed && gamePadState.IsConnected && gpoldState.Buttons.Start == ButtonState.Released))
            {
                if (PKeyPressed == false)
                {
                    if (Lv == 1)
                    {
                        GState = (int)GameState.Paused;
                    }
                    else if (Lv == 2)
                    {
                        GState = (int)GameState.Paused1;
                    }
                    PKeyPressed = true;
                }
                else
                {
                    GState = (int)GameState.Playing;
                    PKeyPressed = false;
                }
            }
            if (GState != (int)GameState.Paused && GState != (int)GameState.Paused1)
            {
                if (gravity == false)
                {
                    if ((keyboardState.IsKeyDown(Keys.Space) && kboldState.IsKeyUp(Keys.Space)) && (keyboardState.IsKeyDown(Keys.Right)) ||
                        (gamePadState.DPad.Right == ButtonState.Pressed && gamePadState.IsConnected && gamePadState.DPad.Left == ButtonState.Released && gamePadState.Buttons.A == ButtonState.Pressed))
                    {
                        CState = (int)CharState.Rightjump;
                        JState = (int)JumpState.Jumping;
                        startPress = gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    else if ((keyboardState.IsKeyDown(Keys.Space) && kboldState.IsKeyUp(Keys.Space)) ||
                        (gamePadState.IsConnected && gpoldState.Buttons.A == ButtonState.Released && gamePadState.Buttons.A == ButtonState.Pressed))
                    {
                        CState = (int)CharState.JumpIdle;
                        JState = (int)JumpState.Jumping;
                        startPress = gameTime.ElapsedGameTime.TotalSeconds;
                    }
                    else if ((keyboardState.IsKeyDown(Keys.Space) && kboldState.IsKeyUp(Keys.Space)) && (keyboardState.IsKeyDown(Keys.Left)) ||
                        (gamePadState.DPad.Left == ButtonState.Pressed && gamePadState.IsConnected && gamePadState.DPad.Right == ButtonState.Released && gamePadState.Buttons.A == ButtonState.Pressed))
                    {
                        CState = (int)CharState.Leftjump;
                        JState = (int)JumpState.Jumping;
                        startPress = gameTime.ElapsedGameTime.TotalSeconds;
                    }
                }

                if (JState == (int)JumpState.Jumping)
                {
                    _elapsedSeconds += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (_elapsedSeconds - startPress >= 0.25f)
                    {
                        _elapsedSeconds = 0;
                        JState = (int)JumpState.nJumping;
                    }
                }

                if (JState == (int)JumpState.nJumping)
                {
                    if (keyboardState.IsKeyDown(Keys.Right) && (keyboardState.IsKeyDown(Keys.RightShift) || keyboardState.IsKeyDown(Keys.LeftShift)) ||
                       (gamePadState.DPad.Right == ButtonState.Pressed && gamePadState.IsConnected && gamePadState.Buttons.X == ButtonState.Pressed))
                    {
                        CState = (int)CharState.Rightrun;
                    }
                    else if (keyboardState.IsKeyDown(Keys.Left) && (keyboardState.IsKeyDown(Keys.RightShift) || keyboardState.IsKeyDown(Keys.LeftShift)) ||
                       (gamePadState.DPad.Left == ButtonState.Pressed && gamePadState.IsConnected && gamePadState.Buttons.X == ButtonState.Pressed))
                    {
                        CState = (int)CharState.Leftrun;
                    }
                    else if (keyboardState.IsKeyDown(Keys.Left) ||
                        (gamePadState.DPad.Left == ButtonState.Pressed && gamePadState.IsConnected))
                    {
                        CState = (int)CharState.Leftwalk;
                    }
                    else if (keyboardState.IsKeyDown(Keys.Right) ||
                       (gamePadState.DPad.Right == ButtonState.Pressed && gamePadState.IsConnected))
                    {
                        CState = (int)CharState.Rightwalk;
                    }
                    else if ((keyboardState.IsKeyUp(Keys.Right)) && (keyboardState.IsKeyUp(Keys.Left)) ||
                        (gamePadState.DPad.Right == ButtonState.Released && gamePadState.IsConnected && gamePadState.DPad.Left == ButtonState.Released))
                    {
                        CState = (int)CharState.Idle;
                    }
                }
            }
            return GState;
        }

        public int UpdateIngameMenu(int GState, GraphicsDevice graphics, Vector2 CamPos, ref int LState)
        {
            if ((keyboardState.IsKeyDown(Keys.Up) && kboldState.IsKeyUp(Keys.Up)) ||
                (gamePadState.DPad.Up == ButtonState.Pressed && gamePadState.IsConnected && gpoldState.DPad.Up == ButtonState.Released))
            {
                MState -= 1;
                if (MState < 0)
                {
                    MState = 2;
                }
            }
            else if ((keyboardState.IsKeyDown(Keys.Down) && kboldState.IsKeyUp(Keys.Down)) ||
                (gamePadState.DPad.Down == ButtonState.Pressed && gamePadState.IsConnected && gpoldState.DPad.Down == ButtonState.Released))
            {
                MState = (MState + 1) % 3;
            }
            else if ((keyboardState.IsKeyDown(Keys.Enter) && kboldState.IsKeyUp(Keys.Enter)) ||
              (gamePadState.Buttons.A == ButtonState.Pressed && gpoldState.Buttons.A == ButtonState.Released) ||
               (mouseState.LeftButton == ButtonState.Pressed && moldState.LeftButton == ButtonState.Released && (MouseCollision(490, 133, 576, 179) ||
                MouseCollision(487, 246, 694, 300) || MouseCollision(481, 366, 569, 415))))
            {
                switch (MState)
                {
                    case 1:
                        GState = (int)GameState.Save;
                        //Save Game 
                        break;
                    case 2:
                        GState = (int)GameState.Menu;
                        LState = (int)Lv1State.Start;
                        break;
                    default:
                        GState = (int)GameState.Playing;
                        break;
                } 
                PKeyPressed = false;
            }
            else if (MouseCollision(490, 133, 576, 179))   
            {
                MState = 0;
            }
            else if (MouseCollision(487, 246, 694, 300)) 
            {
                MState =1;
            }
            else if (MouseCollision(481, 366, 569, 415))
            {
                MState = 2;
            }

            return GState;
        }

        public float Fadein(SpriteBatch spriteBatch, Vector2 campos, float alpha)
        {
             alpha += 0.01f;
             spriteBatch.Draw(black_rect, campos, Color.Black * alpha);
             return alpha;
        }

        public float Fadeout(SpriteBatch spriteBatch, Vector2 campos, float alpha)
        {
            alpha -= 0.01f;
            spriteBatch.Draw(black_rect, campos, Color.Black * alpha);
            return alpha; 
        }

        public string KeyboradInput(string InputText)
        {
            int i = 0; 
            if (Keyboard.GetState().IsKeyUp(lastKey))
            {
                lastKey = Keys.None;
            }
            if (Keyboard.GetState().GetPressedKeys().Length > 0 && lastKey == Keys.None)
            {
                lastKey = Keyboard.GetState().GetPressedKeys()[0];
                if (lastKey == Keys.Back)
                {
                    if (InputText.Length != 0)
                        InputText = InputText.Substring(0, InputText.Length - 1);
                }
                else if (lastKey == Keys.Enter)
                {
                }
                else if (InputText.Length < 10 && lastKey != Keys.Escape && lastKey != Keys.Enter && lastKey != Keys.Up && lastKey != Keys.Down && lastKey != Keys.Left && lastKey != Keys.Right)
                {
                       i = (int)lastKey.GetHashCode() + 32;
                        InputText += (char)i;
                }
            }
            return InputText;
        }

    }
}
