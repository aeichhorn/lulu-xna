﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace LuluXNA
{
    class CLevel2
    {
        public CGameObject[] Levelobj = new CGameObject[7];
        public Texture2D[] Lv2Tex = new Texture2D[12];
        private Vector2 GroundPlan = Vector2.Zero;

        enum GameState { SplashScreen = 1, Menu, InGameMenu, Playing, Paused, LoadGame, Options, Credits, End, Profile, Lv2, Save, Paused1 };
        enum Lv2State { Start, Loading, Switchon, Spider, Playing, End, BPressed };
        private Vector2[] parallax = new Vector2[7];
        public Vector2[] platformpos = new Vector2[4];
        public CMsBox MymsgBox = new CMsBox();
        private Vector2[] Fackelpos = new Vector2[2];
        private int MBoxPosX;
        public bool[] didShowMsgB = new bool[1];
        string[] Messages = new string[1];
        public int LState = (int)Lv2State.Start;
        private CCollision Col;
        public bool save = false;
        private float alpha = 1f;
        public float alpha1 = 1f;
        public float alpha2 = 0f;
        public bool deadfadein = false;
        public bool deadfadeout = false;
        public bool fadein = false;
        public bool fadeout = false;
        public bool rocksound = false;
        private SoundEffect[] effect = new SoundEffect[6];
        SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[6];

        public CLevel2()
        {
            Messages[0] = "";
            GroundPlan = new Vector2(0, 760);
            parallax[0] = new Vector2(0, 0);
            parallax[1] = new Vector2(0.3f);
            parallax[2] = new Vector2(0.5f);
            parallax[3] = new Vector2(0.8f);
            parallax[4] = new Vector2(1f);
            parallax[5] = new Vector2(1.3f);
            parallax[6] = new Vector2(1.5f);
            MBoxPosX = 1677;
            Col = new CCollision();
            for (int i = 0; i < Levelobj.Length; i++)
            {
                Levelobj[i] = new CGameObject();
            }
            Levelobj[2].Pos = new Vector2(-125, 300);
            Levelobj[1].Pos = new Vector2(3072, 300);
            Levelobj[5].Pos = new Vector2(829, 639);
            Levelobj[4].Pos = new Vector2(2667, 408);
            Levelobj[3].Pos = new Vector2(2315, 377);
            Levelobj[0].Pos = new Vector2(2733, -100);
             platformpos[0] = new Vector2(829, 639);
             platformpos[1] = new Vector2(1463, 595);
             platformpos[2] = new Vector2(1989, 449);
             platformpos[3] = new Vector2(2649, 613);
             didShowMsgB[0] = false;
             Fackelpos[0] = new Vector2(895, 399);
             Fackelpos[1] = new Vector2(1809, 362);
        }

       public void LoadContent(ContentManager myContent)
       {
           MymsgBox.LoadContent(myContent);
           Lv2Tex[0] = myContent.Load<Texture2D>("Level2/Lv2_e1");
           Lv2Tex[1] = myContent.Load<Texture2D>("Level2/Lv2_e2");
           Lv2Tex[2] = myContent.Load<Texture2D>("Level2/Lv2_e3");
           Lv2Tex[3] = myContent.Load<Texture2D>("Level2/Lv2_gate2");
           Lv2Tex[4] = myContent.Load<Texture2D>("Level2/Lv2_oe");
           Lv2Tex[5] = myContent.Load<Texture2D>("Level1/PressB");
           Lv2Tex[6] = myContent.Load<Texture2D>("Level2/Lv2_spider_dead");
           Lv2Tex[7] = myContent.Load<Texture2D>("Level1/checkpoint");
           Lv2Tex[8] = myContent.Load<Texture2D>("Level2/Lv2_End");
           Lv2Tex[9] = myContent.Load<Texture2D>("Level0/red");
           Lv2Tex[10] = myContent.Load<Texture2D>("Level0/green");

           Levelobj[0].Texture = myContent.Load<Texture2D>("Level2/Lv2_stone");
           Levelobj[1].Texture = myContent.Load<Texture2D>("Level2/Lv2_bush");
           Levelobj[2].Texture = myContent.Load<Texture2D>("Level2/Lv2_gate1");
           Levelobj[3].Texture = myContent.Load<Texture2D>("Level2/Lv2_switch");
           Levelobj[4].Texture = myContent.Load<Texture2D>("Level2/Lv2_spider");
           Levelobj[5].Texture = myContent.Load<Texture2D>("Level2/Lv2_platform");
           Levelobj[6].Texture = myContent.Load<Texture2D>("Level2/Lv2_Fackel");
           effect[0] = myContent.Load<SoundEffect>("Audio/spider");
           soundEffectInstance[0] = effect[0].CreateInstance();
           soundEffectInstance[0].IsLooped = false;
           effect[1] = myContent.Load<SoundEffect>("Audio/torch");
           soundEffectInstance[1] = effect[1].CreateInstance();
           soundEffectInstance[1].IsLooped = false;
           effect[2] = myContent.Load<SoundEffect>("Audio/switch");
           soundEffectInstance[2] = effect[2].CreateInstance();
           soundEffectInstance[2].IsLooped = false;
           effect[3] = myContent.Load<SoundEffect>("Audio/rock");
           soundEffectInstance[3] = effect[3].CreateInstance();
           soundEffectInstance[3].IsLooped = false;
           effect[4] = myContent.Load<SoundEffect>("Audio/spiderdead");
           soundEffectInstance[4] = effect[4].CreateInstance();
           soundEffectInstance[4].IsLooped = false;
           effect[5] = myContent.Load<SoundEffect>("Audio/darkforest");
           soundEffectInstance[5] = effect[5].CreateInstance();
           soundEffectInstance[5].IsLooped = true;
       }

       public int InitializeLv2(ref CPlayerdata Player){
           Player.msmainChar.Position = new Vector2(300, 620);
           save = false;
           Levelobj[3].Pos = new Vector2(2315, 377);
           Levelobj[0].Pos = new Vector2(2733, -100);
           didShowMsgB[0] = false;
           MymsgBox.showMsgB = false;
           MymsgBox.Count = 0;
           fadein = false;
           fadeout = false;
           rocksound = false;
           deadfadein = false;
           deadfadeout = false;
           alpha = 1f;
           alpha1 = 1f;
           alpha2 = 0f;
           return (int)Lv2State.Playing;
       }

       public int UpdateLevel2(CPlayerdata Player, CControls Controller, int GState, GameTime gameTime, CCamera2D cam, GraphicsDevice graphics, int LvState, ref CLevel1 Lv1, ref CMenu MMenu)
       {
            try
            {
                //             startPress = gameTime.ElapsedGameTime.TotalSeconds;
                //             _elapsedSeconds += (float)gameTime.ElapsedGameTime.TotalSeconds;
                //             if (_elapsedSeconds - startPress >= 1)
                //             {
                //                 _elapsedSeconds = 0;
                //             }               LState = LvState;
                LState = LvState;
               if (LState == (int)Lv2State.Start)
               {
                   LState = InitializeLv2(ref Player);
               }
               if (Player.m_bSound[Player.currentProfile] == true)
               {
                   if (soundEffectInstance[5].State == SoundState.Stopped)
                   {
                       soundEffectInstance[5].Play();
                   }
               }
               GState = Controller.GetInput(GState, gameTime, Player.gravity, Player.Lv);
               if (LState == (int)Lv2State.End)
               {
                   if (Controller.BPressed() == true)
                   {
                       LState = (int)Lv2State.BPressed;
                   }
               } 
               
               if (LState == (int)Lv2State.BPressed)
               {
                  return (int)GameState.Menu;
               }
               else if (GState != (int)GameState.Paused1)
               {
                    Player.PoldPos = Player.msmainChar.Sprite.Position;
                    MymsgBox.UpdateMsgBox(Controller, cam, ref didShowMsgB);
                    Player.msmainChar.Update(gameTime);
                if (MymsgBox.showMsgB != true)
                {
                       if (LState == (int)Lv2State.Playing)
                       {
                           if (Player.msmainChar.Position.X >= 2295 && Player.msmainChar.Position.X <= 2377)
                           {
                               if (Controller.BPressed() == true)
                               {
                                   LState = (int)Lv2State.Switchon;
                                   Levelobj[3].Pos.Y += 25;
                                   if (Player.m_bSound[Player.currentProfile] == true)
                                   {
                                       if (soundEffectInstance[2].State == SoundState.Stopped)
                                       {
                                           soundEffectInstance[2].Play();
                                       }
                                   }
                               }
                           }
                           else if (Player.msmainChar.Position.X >= 2013 && Player.msmainChar.Position.X <= 2113)
                           {
                               if (save != true)
                               {
                                   Player.saveGame(Lv1.LState, LState, GState, (int)Lv1.Levelobj[8].Pos.X, Lv1.MymsgBox.Count, Lv1.Raven, Lv1.msgcount, Lv1.didShowMsgB, Lv1.Speaking, Lv1.gate, ref Lv1.RavenMsg.show,
                                       ref save, ref Levelobj, ref didShowMsgB[0], ref MymsgBox.Count);
                                   if (Player.m_bSound[Player.currentProfile] == true)
                                   {
                                       if (MMenu.soundEffectInstance[1].State == SoundState.Stopped)
                                       {
                                           MMenu.soundEffectInstance[1].Play();
                                       }
                                   }
                                   save = true;
                               }
                           } else if (Col.Intersects(Player.msmainChar.BoundingBox, Levelobj[4].BoundingRectangle) == true)
                           {
                               Player.isAlive = false;
                           }
                           if (Player.msmainChar.Position.X >= 1677)
                           {
                               if (Player.m_bSound[Player.currentProfile] == true)
                               {
                                   if (soundEffectInstance[0].State == SoundState.Stopped)
                                   {
                                       soundEffectInstance[0].Play();
                                   }
                               }
                           }
                       }
                       else if (LState == (int)Lv2State.Spider && Player.msmainChar.Sprite.Position.X > 2850 && fadein == true)
                       {
                           LState = (int)Lv2State.End;
                       }
                       else if (LState == (int)Lv2State.Switchon || LState == (int)Lv2State.Spider)
                       {
                           if (Levelobj[0].Pos.Y < 768)
                           {
                               Levelobj[0].Pos.Y += 6;
                           }
                           //if stone intersects with spider
                           if (Col.Intersects(Levelobj[0].BoundingRectangle, Levelobj[4].BoundingRectangle) == true)
                           {
                               if (Player.m_bSound[Player.currentProfile] == true)
                               {
                                   if (soundEffectInstance[3].State == SoundState.Stopped)
                                   {
                                       if (rocksound == false)
                                       {
                                           soundEffectInstance[3].Play();
                                           rocksound = true;
                                       }
                                   }
                                   if (soundEffectInstance[4].State == SoundState.Stopped)
                                   {
                                       soundEffectInstance[4].Play();
                                   }
                               }
                               LState = (int)Lv2State.Spider;
                           }
                       }

                       if (Player.isAlive == false)
                       {
                           if (deadfadein == true)
                           {
                               if (save == true)
                               {
                                   Player.Reset(new Vector2(2013, 300));
                               }
                               else
                               {
                                   bool reset = false;
                                   for (int i = 0; i < platformpos.Length - 1; i++ )
                                   {
                                       if (platformpos[i].X < Player.msmainChar.Position.X)
                                       {
                                           if (platformpos[i].X <= Player.msmainChar.Position.X && (platformpos[i].X + Levelobj[5].Texture.Width) >= Player.msmainChar.Position.X)
                                           {
                                               Player.Reset(new Vector2(platformpos[i].X + Levelobj[5].Texture.Width / 2, platformpos[i].Y - Player.msmainChar.Sprite.Height));
                                               reset = true;
                                               break;
                                           }
                                       }
                                   }
                                   if (reset == false)
                                   {
                                       Player.Reset(new Vector2(300 ,620));
                                   }
                               }
                           }
                       }
                       if (Player.msmainChar.Sprite.Position.X >= MBoxPosX && Player.msmainChar.Sprite.Position.X <= MBoxPosX + 50 && didShowMsgB[0] == false)
                       {
                           MymsgBox.showMsgB = true;
                       }
                       // Updates your camera to lock on the character
                       cam.Update(cam, Player);
                       if (deadfadeout == false)
                       {
                           Player.UpdateChar(Controller.CState, Controller.JState, gameTime, GroundPlan, cam, Player, Col, Levelobj, ref platformpos);
                       }   
                }
               }
               else
               {
                   GState = Controller.UpdateIngameMenu(GState, graphics, cam.Pos, ref LState);
               }
            }
            catch (System.IndexOutOfRangeException)
            {
               return (int)GameState.Menu;
            }

           return GState;
       }

        public int DrawLevel2(SpriteBatch spriteBatch, CCamera2D cam, CPlayerdata Player, CMenu MMenu, int GState, CControls Controller)
        {
            if (LState != (int)Lv2State.End)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[3]));
                    //Background Layer
                    spriteBatch.Draw(Lv2Tex[0], new Vector2(0,0), Color.White);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
                    //draw platforms
                    for (int i = 0; i < platformpos.Length; i++ )
                    {
                        Levelobj[5].Pos = platformpos[i];
                        spriteBatch.Draw(Levelobj[5].Texture, Levelobj[5].Pos, Color.White);
                    }
                    //draw Switch
                    spriteBatch.Draw(Levelobj[3].Texture, Levelobj[3].Pos, Color.White);
                    spriteBatch.Draw(Levelobj[0].Texture, Levelobj[0].Pos, Color.White);
                    spriteBatch.Draw(Lv2Tex[3], new Vector2(-110, 300), Color.White);
                    spriteBatch.Draw(Levelobj[2].Texture, Levelobj[2].Pos, Color.White);
                    //Press B
                    if ((Player.msmainChar.Position.X >= 2295 && Player.msmainChar.Position.X <= 2377) && LState == (int)Lv2State.Playing)
                    {
                        spriteBatch.Draw(Lv2Tex[5], new Vector2(Levelobj[3].Pos.X - 20, Levelobj[3].Pos.Y - 100), Color.White); 
                    } 
                    //draw Player
                    Player.msmainChar.Draw(spriteBatch);
                    spriteBatch.End();
                    //draw torch
                    alpha = (alpha + 0.1f) % 1;
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, cam.GetViewMatrix(parallax[4]));
                    for (int j = 0; j < Fackelpos.Length; j++)
                    {
                        spriteBatch.Draw(Lv2Tex[10], new Vector2(Fackelpos[j].X - 110, Fackelpos[j].Y - 50), Color.White * alpha);
                        spriteBatch.Draw(Lv2Tex[9], new Vector2(Fackelpos[j].X - 110, Fackelpos[j].Y - 50), Color.White);
                    }
                    spriteBatch.End();
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
                    for (int i = 0; i < Fackelpos.Length; i++)
                    {
                        spriteBatch.Draw(Levelobj[6].Texture, Fackelpos[i], Color.White);
                        Levelobj[6].Pos = Fackelpos[i];
                        if (Col.Intersects(Player.msmainChar.BoundingBox, Levelobj[6].BoundingRectangle))
                        {
                            if (Player.m_bSound[Player.currentProfile] == true)
                            {
                                if (soundEffectInstance[1].State == SoundState.Stopped)
                                {
                                    soundEffectInstance[1].Play();
                                }
                            }
                        }
                    }
                    //Checkpoint
                    if (save == true)
                    {
                        spriteBatch.Draw(Lv2Tex[7], new Vector2(2013, 300), Color.White);
                    }
                    spriteBatch.Draw(Lv2Tex[2], new Vector2(0 ,0), Color.White);
                    spriteBatch.Draw(Levelobj[1].Texture, Levelobj[1].Pos, Color.White);
                    if (LState != (int)Lv2State.Spider)
                    {
                        spriteBatch.Draw(Levelobj[4].Texture, Levelobj[4].Pos, Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(Lv2Tex[6], new Vector2(Levelobj[4].Pos.X, Levelobj[4].Pos.Y + 100), Color.White);
                    }
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, null, null, null, cam.GetViewMatrix(parallax[3]));
                     //Fog Layer
                     spriteBatch.Draw(Lv2Tex[1], new Vector2(0 ,0), Color.White);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[6]));
                     //Front Layer
                spriteBatch.Draw(Lv2Tex[4],new Vector2(0 ,0), Color.White);
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));

                if (GState == (int)GameState.Paused1)
                {
                    MMenu.DrawIngameMenu(spriteBatch, Controller.MState, cam, MMenu, Player, parallax[4]);
                }
                if (MymsgBox.showMsgB == true)
                {
                    if (MymsgBox.Count == 0)
                        Messages[0] = "You look delicious! Hehe..."; 
                        MymsgBox.DrawMsgBox(spriteBatch, cam, Messages, true, Controller, parallax[4]);
                }

                if (fadeout != true)
                {
                    if (alpha1 <= 0)
                    {
                        fadeout = true;
                    }
                    alpha1 = Controller.Fadeout(spriteBatch, cam.Pos, alpha1);
                }
                if (LState == (int)Lv2State.Spider && Player.msmainChar.Sprite.Position.X > 2900 && fadein != true)
                {
                    if (alpha1 >= 1)
                    {
                        fadein = true;
                    }
                    alpha1 = Controller.Fadein(spriteBatch, cam.Pos, alpha1);
                }
                if (Player.isAlive == false)
                {
                    if (alpha2 >= 1)
                    {
                        deadfadein = true;
                    }
                    alpha2 = Controller.Fadein(spriteBatch, cam.Pos, alpha2);
                }
                if (deadfadein == true)
                {
                    if (alpha2 <= 0)
                    {
                        deadfadein = false;
                        deadfadeout = false;
                    }
                    alpha2 = Controller.Fadeout(spriteBatch, cam.Pos, alpha2);
                }
                spriteBatch.End();
            }
            else
            {
                soundEffectInstance[5].Stop();
                soundEffectInstance[0].Stop();
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
                    spriteBatch.Draw(Lv2Tex[8], new Vector2(cam.Pos.X, cam.Pos.Y), Color.White);
                spriteBatch.End(); 
            }
            return GState;
        }
    }
}
