﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LuluXNA
{
    class CMsBox
    {
        public Texture2D[] MsgB = new Texture2D[2];
        public bool showMsgB = false;
        public SpriteFont font, fontbig;
        public bool show = false;
        const int LINE = 35;
        public int Lineheight = LINE;
        private Vector2 LPOS = new Vector2(45, 80);
        public Vector2 LinePos = new Vector2(45, 80);
        public int Count = 0;
        float _elapsedSeconds = 0f;
        double startPress = 0f;

        public CMsBox()
        {
        }

        public void LoadContent(ContentManager myContent)
        {
            MsgB[0] = myContent.Load<Texture2D>("MsgBox/MessageBox");
            MsgB[1] = myContent.Load<Texture2D>("MsgBox/okbtn");
            font = myContent.Load<SpriteFont>("Fonts/Arial");
            fontbig = myContent.Load<SpriteFont>("Fonts/Arialbig");
        }

        public bool CheckMsgBox(CCamera2D cam, CControls Controller)
        {
            if ((Controller.keyboardState.IsKeyDown(Keys.Enter) && Controller.kboldState.IsKeyUp(Keys.Enter)) ||
                (Controller.gamePadState.Buttons.B == ButtonState.Pressed && Controller.gpoldState.Buttons.B == ButtonState.Released) ||
                (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released &&
                Controller.MouseCollision(930, 69, 997, 169)))
            {
                return false;
            }
            return true;
        }

        public void UpdateMsgBox(CControls Controller, CCamera2D cam, ref bool[] didShow)
        {
            if (showMsgB == true)
            {
                if (CheckMsgBox(cam, Controller) == false)
                {
                    showMsgB = false;
                    didShow[Count] = true;
                    Count++;
                }
            }
        }

        public bool UpdateMsgBox(CControls Controller, CCamera2D cam, bool showBox, GameTime gameTime)
        {
            show = showBox;
            if (show == true)
            {
                _elapsedSeconds += (float)gameTime.ElapsedGameTime.TotalSeconds;
                startPress = gameTime.ElapsedGameTime.TotalSeconds;
                 if (_elapsedSeconds - startPress >= 1)
                 {
                    if (CheckMsgBox(cam, Controller) == false)
                    {
                        show = false;
                        showBox = false;
                    }
                    _elapsedSeconds = 0;
                 }
            }
            return showBox;
        }

        public void DrawMsgBox(SpriteBatch spriteBatch, CCamera2D cam, String[] Messages, bool bigfont, CControls Controller, Vector2 parallax)
        {
            if (showMsgB == true || show == true)
            {
                spriteBatch.Draw(MsgB[0], cam.Pos, Color.White);
                foreach (String Msg in Messages)
                {
                        if (Msg != null)
                        {
                            if (bigfont == true)
                            {
                                spriteBatch.DrawString(fontbig, Msg, cam.ScreenToWorld(new Vector2(LinePos.X, LinePos.Y), parallax), Color.White);
                            }
                            else
                            {
                                spriteBatch.DrawString(font, Msg, cam.ScreenToWorld(new Vector2(LinePos.X, LinePos.Y), parallax), Color.White);
                            }
                            LinePos.Y += Lineheight;
                        }
                }
                LinePos = LPOS;
                if (Controller.MouseCollision(930, 69, 997, 169))
                {
                    spriteBatch.Draw(MsgB[1], cam.ScreenToWorld(new Vector2(920, 120), parallax), Color.White);
                }
            }
       }
    }
}
