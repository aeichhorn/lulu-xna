﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.Xml;
using Microsoft.Xna.Framework.Audio;
using System.Runtime.InteropServices;

namespace LuluXNA
{
    class CPlayerdata : CProfiledata
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern uint MessageBox(IntPtr hWnd, String text, String caption, uint type);

        enum CharState { Idle = 0, Leftwalk, Rightwalk, Rightrun, Leftrun, Rightjump, Leftjump, JumpIdle };
        enum JumpState { Jumping, Falling, nJumping };
        public Texture2D mainChar;
        public CMobileSprites msmainChar;
        SpriteBatch spriteBatch;
        GraphicsDeviceManager graphics;
        public Vector2 PoldPos = new Vector2(200, 620);
        public CFrameAnimation mFrameChar;
        public bool isAlive = true;
        public Vector2 velocity = new Vector2(0, 0);
        public bool gravity = false;
        public int JumpHeight = 150;
        const int MOVE_LEFT = -1;
        const int MOVE_RIGHT = 1;
        public Vector2 mDirection = Vector2.Zero;
        XmlTextWriter writer;
        XmlReader reader;
        public int Lv = 1;
        private SoundEffect[] effect = new SoundEffect[2];
        SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[2];

        public CPlayerdata(GraphicsDeviceManager curgraphics, SpriteBatch curspriteBatch)
        {
            spriteBatch = curspriteBatch;
            graphics = curgraphics;
       }

        public void LoadPlayerContent(ContentManager myContent, Viewport _view)
        {
            mainChar = myContent.Load<Texture2D>(@"MainChar\walkrun_spritesheet");
            msmainChar = new CMobileSprites(mainChar);
            msmainChar.Sprite.AddAnimation("jumprighstart", 0, 0, 170, 73, 5, 0.1f, "flyingright");
            msmainChar.Sprite.AddAnimation("jumpleftstart", 0, 73, 170, 73, 4, 0.1f, "flyingleft");
            msmainChar.Sprite.AddAnimation("flyingright", 170, 282, 170, 73, 3, 0.1f, "landright");
            msmainChar.Sprite.AddAnimation("flyingleft", 170, 355, 170, 73, 3, 0.1f, "landright");
            msmainChar.Sprite.AddAnimation("landright", 1019, 1, 170, 73, 2, 0.2f, "idle");
            msmainChar.Sprite.AddAnimation("landleft", 1019, 73, 170, 73, 2, 0.2f, "idle");
            msmainChar.Sprite.AddAnimation("runright", 0, 0, 170, 73, 8, 0.1f);
            msmainChar.Sprite.AddAnimation("runleft", 0, 73, 170, 73, 8, 0.1f);
            msmainChar.Sprite.AddAnimation("walkright", 0, 146, 170, 73, 8, 0.1f);
            msmainChar.Sprite.AddAnimation("walkleft", 0, 219, 170, 73, 8, 0.1f);
            msmainChar.Sprite.AddAnimation("idle", 0, 292, 80, 90, 1, 0.1f);
            msmainChar.Sprite.CurrentAnimation = "idle";
            msmainChar.IsCollidable = true;
            msmainChar.Position = new Vector2(200, 620);
            msmainChar.Sprite.AutoRotate = false;
            msmainChar.Speed = 3;
            msmainChar.LoopPath = false;
            msmainChar.IsPathing = false;
            msmainChar.IsMoving = false;
            mFrameChar = msmainChar.Sprite.CurrentCFrameAnimation;
            effect[0] = myContent.Load<SoundEffect>("Audio/jump");
            soundEffectInstance[0] = effect[0].CreateInstance();
            soundEffectInstance[0].IsLooped = false;
            effect[1] = myContent.Load<SoundEffect>("Audio/walk");
            soundEffectInstance[1] = effect[1].CreateInstance();
            soundEffectInstance[1].IsLooped = false;
        }

        public void DrawCharacter()
        {
            msmainChar.Draw(spriteBatch);
        }

        public void UpdateMoveChar(int CState, int JState, GameTime gameTime, Vector2[] GroundPos, CCamera2D cam, CPlayerdata Player, CCollision Col, CGameObject[] Levelobj, bool gate)
        {
            bool onPlatform = false;
            mFrameChar = msmainChar.Sprite.CurrentCFrameAnimation;
            msmainChar.Position += velocity;

            if (JState == (int)JumpState.Jumping)
            {
                velocity.Y += 0.10f * 1;
            } else if (JState == (int)JumpState.nJumping)
            {
                velocity.Y = 0f;
            }
             
            switch (CState)
                {
                    case (int)CharState.JumpIdle:
                            if (mDirection.X == MOVE_RIGHT)
                            {
                                 if (msmainChar.Sprite.CurrentAnimation != "flyingright")
                                 {
                                     msmainChar.Sprite.CurrentAnimation = "flyingright";
                                 }
                                 if (Col.PixelperPixelCollision(msmainChar, Levelobj[6].Texture, msmainChar.Position, Levelobj[6].Pos) != true || gate == true)
                                 {
                                        msmainChar.Sprite.MoveBy(0, -10);
                                        velocity.Y = -6f;
                                        velocity.X = 2f;
                                        if (Player.m_bSound[Player.currentProfile] == true)
                                        {
                                            if (soundEffectInstance[0].State == SoundState.Stopped)
                                            {
                                                soundEffectInstance[0].Play();
                                            }
                                        }
                                 }
                                 else
                                 {
                                     velocity.X = 0f;
                                 }
                            }
                            else
                            {
                                 if (msmainChar.Sprite.CurrentAnimation != "flyingleft")
                                 {
                                     msmainChar.Sprite.CurrentAnimation = "flyingleft";
                                 }
                                 if (Player.m_bSound[Player.currentProfile] == true)
                                 {
                                     if (soundEffectInstance[0].State == SoundState.Stopped)
                                     {
                                         soundEffectInstance[0].Play();
                                     }
                                 }
                                 if (Col.PixelperPixelCollision(msmainChar, Levelobj[5].Texture, msmainChar.Position, Levelobj[5].Pos) != true)
                                {
                                    msmainChar.Sprite.MoveBy(-0, -17);
                                    velocity.X = -2f;
                                    velocity.Y = -5f;
                                }
                                else
                                {
                                    velocity.X = 0f;
                                }
                            }
                        break;
                    case (int)CharState.Rightjump:
                         if (msmainChar.Sprite.CurrentAnimation != "flyingright")
                         {
                             msmainChar.Sprite.CurrentAnimation = "flyingright";
                         }
                         if (Player.m_bSound[Player.currentProfile] == true)
                         {
                             if (soundEffectInstance[0].State == SoundState.Stopped)
                             {
                                 soundEffectInstance[0].Play();
                             }
                         }
                         if (Col.PixelperPixelCollision(msmainChar, Levelobj[6].Texture, msmainChar.Position, Levelobj[6].Pos) != true || gate == true)
                        {
                            msmainChar.Sprite.MoveBy(0, -17);  
                            velocity.X = 3f;
                            velocity.Y = -5f;
                        }
                        else
                        {
                            velocity.X = 0f;
                        }
                        mDirection.X = MOVE_RIGHT;
                        break;
                    case (int)CharState.Leftjump:
                            if (msmainChar.Sprite.CurrentAnimation != "flyingleft")
                             {
                                 msmainChar.Sprite.CurrentAnimation = "flyingleft";
                             }
                            if (Player.m_bSound[Player.currentProfile] == true)
                            {
                                if (soundEffectInstance[0].State == SoundState.Stopped)
                                {
                                    soundEffectInstance[0].Play();
                                }
                            }
                        if (Col.PixelperPixelCollision(msmainChar, Levelobj[5].Texture, msmainChar.Position, Levelobj[5].Pos) != true)
                        {
                            msmainChar.Sprite.MoveBy(0, -17);  
                            velocity.X = -3f;
                            velocity.Y = -5f;
                        }
                        else
                        {
                            velocity.X = 0f;
                        }
                         mDirection.X = MOVE_LEFT;
                        break;

                    case (int)CharState.Rightrun:
                            if (msmainChar.Sprite.CurrentAnimation != "runright")
                            {
                                msmainChar.Sprite.CurrentAnimation = "runright";
                            }
                            if (Col.PixelperPixelCollision(msmainChar, Levelobj[6].Texture, msmainChar.Position, Levelobj[6].Pos) != true || gate == true)
                            {
                                if (Player.m_bSound[Player.currentProfile] == true)
                                {
                                    if (soundEffectInstance[1].State == SoundState.Stopped)
                                    {
                                        soundEffectInstance[1].Play();
                                    }
                                }
                                msmainChar.Sprite.MoveBy(5, 0);
                                velocity.X = 4f;
                            }
                            else
                            {
                                soundEffectInstance[1].Stop();
                                velocity.X = 0f;
                            }
                             mDirection.X = MOVE_RIGHT;
                        break;

                    case (int)CharState.Leftrun:
                            if (msmainChar.Sprite.CurrentAnimation != "runleft")
                            {
                                msmainChar.Sprite.CurrentAnimation = "runleft";
                            }
                        if (Col.PixelperPixelCollision(msmainChar, Levelobj[5].Texture, msmainChar.Position, Levelobj[5].Pos) != true)
                        {
                            if (Player.m_bSound[Player.currentProfile] == true)
                            {
                                if (soundEffectInstance[1].State == SoundState.Stopped)
                                {
                                    soundEffectInstance[1].Play();
                                }
                            }
                            msmainChar.Sprite.MoveBy(-5, 0);
                            velocity.X = -4f;
                        }
                        else
                        {
                            velocity.X = 0f;
                        }
                        mDirection.X = MOVE_LEFT;
                        break;

                    case (int)CharState.Leftwalk:
                           if (msmainChar.Sprite.CurrentAnimation != "walkleft")
                            {
                                msmainChar.Sprite.CurrentAnimation = "walkleft";
                            }
                        if (Col.PixelperPixelCollision(msmainChar, Levelobj[5].Texture, msmainChar.Position, Levelobj[5].Pos) != true)
                        {
                            if (Player.m_bSound[Player.currentProfile] == true)
                            {
                                if (soundEffectInstance[1].State == SoundState.Stopped)
                                {
                                    soundEffectInstance[1].Play();
                                }
                            }
                            msmainChar.Sprite.MoveBy(-2, 0);
                            velocity.X = -3f;
                        }
                        else
                        {
                            velocity.X = 0f;
                        }
                        mDirection.X = MOVE_LEFT;
                        break;

                    case (int)CharState.Rightwalk:
                           if (msmainChar.Sprite.CurrentAnimation != "walkright")
                            {
                                msmainChar.Sprite.CurrentAnimation = "walkright";
                            }
                           if (Col.PixelperPixelCollision(msmainChar, Levelobj[6].Texture, msmainChar.Position, Levelobj[6].Pos) != true || gate == true)
                        {
                            if (Player.m_bSound[Player.currentProfile] == true)
                            {
                                if (soundEffectInstance[1].State == SoundState.Stopped)
                                {
                                    soundEffectInstance[1].Play();
                                }
                            }
                            msmainChar.Sprite.MoveBy(+2, 0);
                            velocity.X = 3f;
                        }
                        else
                        {
                            velocity.X = 0f;
                        }
                        mDirection.X = MOVE_RIGHT;
                        break;

                    default:
                            if (msmainChar.Sprite.CurrentAnimation != "idle")
                            {
                                msmainChar.Sprite.CurrentAnimation = "idle";
                            }
                            if (soundEffectInstance[1].State != SoundState.Stopped)
                            {
                                soundEffectInstance[1].Stop();
                            }
                            velocity.X = 0f;
                       break;
                }
            if (Col.IsOnTopOf(Player.msmainChar.BoundingBox, Levelobj[10].BoundingRectangle) != true)
            {
                if (Col.IsOnTheLeft(Player.msmainChar.BoundingBox, Levelobj[8].BoundingRectangle, ref mDirection.X) && Levelobj[8].Pos.X > 100)
              {
                    Levelobj[8].Pos.X = msmainChar.Position.X + msmainChar.Sprite.Width;
              }
                else if (Col.IsOnTheRight(Player.msmainChar.BoundingBox, Levelobj[8].BoundingRectangle, ref mDirection.X))
              {
                  Levelobj[8].Pos.X = msmainChar.Position.X - Levelobj[8].Texture.Width;
              }
            } else 
            {
                JState = (int)JumpState.nJumping;
                onPlatform = true;
                gravity = false;
            }

            //Check if we are standing on a Platform
             if (Col.IsOnTopOf(Player.msmainChar.BoundingBox, Levelobj[9].BoundingRectangle) )
             {
                 JState = (int)JumpState.nJumping;
                 onPlatform = true;
                 gravity = false;
             } 

             foreach (Vector2 Pos in GroundPos)
              {
                  if (msmainChar.Sprite.Position.X >= Pos.Y || msmainChar.Sprite.Position.X <= Pos.X)
                  {
                      gravity = true;
                      break;
                  }
              }

             if (msmainChar.Position.Y < 620 && onPlatform == false)
                 gravity = true;
             if (gravity == true)
             {
                 msmainChar.Sprite.MoveBy(0, 6);
             }
             if (msmainChar.Sprite.Position.Y >= 620)
             {
                 msmainChar.Sprite.SetPositionY(620);
                 gravity = false;
             }
             onPlatform = false;
        }

        /// <summary>
        /// Resets the player to life.
        /// </summary>
        /// <param name="position">The position to come to life at.</param>
        public void Reset(Vector2 position)
        {
            msmainChar.Position = position;
            msmainChar.Sprite.CurrentAnimation = "idle";
            isAlive = true;
        }

        public int saveGame(int LState1, int LState2, int GState, int BuschPosX, int MBoxCount, CMobileSprites Raven, int MSGCOUNT, bool[] didShowMsg, 
             bool Speaking, bool gate, ref bool show, ref bool save, ref CGameObject[] Lv2GameObj, ref bool didshowMsg2, ref int MsCount)
        {
            try
            {
                writer = new XmlTextWriter("Content/Data/Save" + currentProfile + ".xml", null);
                writer.WriteStartDocument();
                writer.WriteStartElement("mainnode");

                writer.WriteStartElement("mainCharPosX");
                writer.WriteValue(msmainChar.Position.X);
                writer.WriteEndElement();
                writer.WriteStartElement("mainCharPosY");
                writer.WriteValue(msmainChar.Position.Y);
                writer.WriteEndElement();
                writer.WriteStartElement("Level");
                writer.WriteValue(Lv);
                writer.WriteEndElement();
                writer.WriteStartElement("GameState");
                writer.WriteValue(GState);
                writer.WriteEndElement();
                writer.WriteStartElement("LState1");
                writer.WriteValue(LState1);
                writer.WriteEndElement();
                writer.WriteStartElement("LState2");
                writer.WriteValue(LState2);
                writer.WriteEndElement();

                for (int i = 0; i < MSGCOUNT; i++)
                {
                    string name = "didShowMsg" + i;
                    writer.WriteStartElement(name);
                    writer.WriteValue(didShowMsg[i]);
                    writer.WriteEndElement();
                }

                writer.WriteStartElement("BuschposX");
                writer.WriteValue(BuschPosX);
                writer.WriteEndElement();
                writer.WriteStartElement("MsBoxCount");
                writer.WriteValue(MBoxCount);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenPosX");
                writer.WriteValue(Raven.Position.X);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenPosY");
                writer.WriteValue(Raven.Position.Y);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenTargetX");
                writer.WriteValue(Raven.Target.X);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenTargetY");
                writer.WriteValue(Raven.Target.Y);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenIsMoving");
                writer.WriteValue(Raven.IsMoving);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenCurrentAni");
                writer.WriteString(Raven.Sprite.CurrentAnimation);
                writer.WriteEndElement();
                writer.WriteStartElement("Speaking");
                writer.WriteValue(Speaking);
                writer.WriteEndElement();
                writer.WriteStartElement("gate");
                writer.WriteValue(gate);
                writer.WriteEndElement();
                writer.WriteStartElement("RavenmsgShow");
                writer.WriteValue(show);
                writer.WriteEndElement();
                writer.WriteStartElement("save");
                writer.WriteValue(save);
                writer.WriteEndElement();
                writer.WriteStartElement("LObject3X");
                writer.WriteValue(Lv2GameObj[3].Pos.X);
                writer.WriteEndElement();
                writer.WriteStartElement("LObject3Y");
                writer.WriteValue(Lv2GameObj[3].Pos.Y);
                writer.WriteEndElement();
                writer.WriteStartElement("LObject0X");
                writer.WriteValue(Lv2GameObj[0].Pos.X);
                writer.WriteEndElement();
                writer.WriteStartElement("LObject0Y");
                writer.WriteValue(Lv2GameObj[0].Pos.Y);
                writer.WriteEndElement();
                writer.WriteStartElement("didshowMsg");
                writer.WriteValue(didshowMsg2);
                writer.WriteEndElement();
                writer.WriteStartElement("MsCount2");
                writer.WriteValue(MsCount);
                writer.WriteEndElement();

                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }
            catch (System.NullReferenceException)
            {
              //  writer.Close();
                MessageBox(new IntPtr(0), "An Error occured! Back to Menu!", "Null Reference Exception", 0);
                return 2;
            }
            catch (System.UnauthorizedAccessException)
            {
                MessageBox(new IntPtr(0), "Couldn't get access to the file! Can't save the game!", "Unauthorized Access Exception", 0);
            }
            catch (System.Xml.XmlException)
            {
                writer.Close();
                MessageBox(new IntPtr(0), "Couldn't write the file!", "XML Exception", 0);
            }

            return GState;
        }

        public int loadGame(ref int GState, ref int LState1, ref int LState2, ref CMobileSprites Raven, ref bool Speaking, ref bool gate, ref CGameObject Busch, ref bool[] didShowMsgB, int msgcount, 
            ref CMsBox RavenMsg, ref CMsBox MymsgBox, ref bool save, ref CGameObject[] Lv2GameObj, ref bool didshowMsg2, ref int MsCount)
        {
            Vector2 mainCharPos = Vector2.Zero;
            Vector2 RavenPos = Vector2.Zero;
            Vector2 RavenTarget = Vector2.Zero;

            try
            {
                reader = XmlReader.Create("Content/Data/Save" + currentProfile + ".xml");
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "mainCharPosX")
                         mainCharPos.X = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "mainCharPosY")
                          mainCharPos.Y = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Level")
                        Lv = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "GameState")
                          GState = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "LState1")
                          LState1 = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "LState2")
                        LState2 = reader.ReadElementContentAsInt();
                    for (int i = 0; i < msgcount; i++)
                    {
                        string name = "didShowMsg" + i;
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == name)
                            didShowMsgB[i] = reader.ReadElementContentAsBoolean();
                    } 
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "BuschposX")
                       Busch.Pos.X = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "MsBoxCount")
                        MymsgBox.Count = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenPosX")
                        RavenPos.X = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenPosY")
                        RavenPos.Y = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenTargetX")
                        RavenTarget.X = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenTargetY")
                        RavenTarget.Y = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenIsMoving")
                        Raven.IsMoving = reader.ReadElementContentAsBoolean();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenCurrentAni")
                        Raven.Sprite.CurrentAnimation = reader.ReadElementContentAsString();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Speaking")
                        Speaking = reader.ReadElementContentAsBoolean();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "gate")
                        gate = reader.ReadElementContentAsBoolean();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "RavenmsgShow")
                        RavenMsg.show = reader.ReadElementContentAsBoolean();

                    //ref bool save, ref CGameObject[] Lv2GameObj, ref bool didshowMsg2, ref int MsCount
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "save")
                        save = reader.ReadElementContentAsBoolean();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "LObject3X")
                        Lv2GameObj[3].Pos.X = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "LObject3Y")
                        Lv2GameObj[3].Pos.Y = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "LObject0X")
                        Lv2GameObj[0].Pos.Y = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "LObject0Y")
                        Lv2GameObj[0].Pos.Y = reader.ReadElementContentAsInt();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "didshowMsg")
                        didshowMsg2= reader.ReadElementContentAsBoolean();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "MsCount2")
                        MsCount = reader.ReadElementContentAsInt();
                }
                reader.Close();
                msmainChar.Position = mainCharPos;
                Raven.Position = RavenPos;
                Raven.Target = RavenTarget;
            }
            catch (System.Xml.XmlException)
            {
                reader.Close();
            }
            catch (System.NullReferenceException)
            {
                //reader.Close();
                MessageBox(new IntPtr(0), "An Error occured! Back to Menu!", "Null Reference Exception", 0);
                return 2;
            }
            catch (System.UnauthorizedAccessException)
            {
                MessageBox(new IntPtr(0), "Couldn't get access to the file! Can't save the game!", "Unauthorized Access Exception", 0);
            }
            return GState;
      }

        public void UpdateChar(int CState, int JState, GameTime gameTime, Vector2 GroundPos, CCamera2D cam, CPlayerdata Player, CCollision Col, CGameObject[] Levelobj, ref Vector2[] platformpos)
        {
            bool onPlatform = false;
            mFrameChar = msmainChar.Sprite.CurrentCFrameAnimation;
            msmainChar.Position += velocity;

            if (JState == (int)JumpState.Jumping)
            {
                velocity.Y += 0.10f * 1;
            }
            else if (JState == (int)JumpState.nJumping)
            {
                velocity.Y = 0f;
            }

            switch (CState)
            {
                case (int)CharState.JumpIdle:
                    if (mDirection.X == MOVE_RIGHT)
                    {
                        if (msmainChar.Sprite.CurrentAnimation != "flyingright")
                        {
                            msmainChar.Sprite.CurrentAnimation = "flyingright";
                        }
                        if (Player.m_bSound[Player.currentProfile] == true)
                        {
                            if (soundEffectInstance[0].State == SoundState.Stopped)
                            {
                                soundEffectInstance[0].Play();
                            }
                        }
                        msmainChar.Sprite.MoveBy(0, -10);
                            velocity.Y = -6f;
                            velocity.X = 2f;
                    }
                    else
                    {
                        if (msmainChar.Sprite.CurrentAnimation != "flyingleft")
                        {
                            msmainChar.Sprite.CurrentAnimation = "flyingleft";
                        }
                        if (Col.PixelperPixelCollision(msmainChar, Levelobj[2].Texture, msmainChar.Position, Levelobj[2].Pos) != true)
                        {
                            if (Player.m_bSound[Player.currentProfile] == true)
                            {
                                if (soundEffectInstance[0].State == SoundState.Stopped)
                                {
                                    soundEffectInstance[0].Play();
                                }
                            }
                            msmainChar.Sprite.MoveBy(-0, -17);
                            velocity.X = -2f;
                            velocity.Y = -5f;
                        }
                        else
                        {
                            velocity.X = 0f;
                            velocity.Y = 0f;
                        }
                    }
                    break;
                case (int)CharState.Rightjump:
                    if (msmainChar.Sprite.CurrentAnimation != "flyingright")
                    {
                        msmainChar.Sprite.CurrentAnimation = "flyingright";
                    }
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                        if (soundEffectInstance[0].State == SoundState.Stopped)
                        {
                            soundEffectInstance[0].Play();
                        }
                    }
                    msmainChar.Sprite.MoveBy(0, -17);
                    velocity.X = 3f;
                    velocity.Y = -5f;
                    mDirection.X = MOVE_RIGHT;
                    break;
                case (int)CharState.Leftjump:
                    if (msmainChar.Sprite.CurrentAnimation != "flyingleft")
                    {
                        msmainChar.Sprite.CurrentAnimation = "flyingleft";
                    }
                    if (Col.PixelperPixelCollision(msmainChar, Levelobj[2].Texture, msmainChar.Position, Levelobj[2].Pos) != true)
                    {
                        if (Player.m_bSound[Player.currentProfile] == true)
                        {
                            if (soundEffectInstance[0].State == SoundState.Stopped)
                            {
                                soundEffectInstance[0].Play();
                            }
                        }
                        msmainChar.Sprite.MoveBy(0, -17);
                        velocity.X = -3f;
                        velocity.Y = -5f;
                    }
                    else
                    {
                        velocity.X = 0f;
                        velocity.Y = 0f;
                    }

                    mDirection.X = MOVE_LEFT;
                    break;

                case (int)CharState.Rightrun:
                    if (msmainChar.Sprite.CurrentAnimation != "runright")
                    {
                        msmainChar.Sprite.CurrentAnimation = "runright";
                    }
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                        if (soundEffectInstance[1].State == SoundState.Stopped)
                        {
                            soundEffectInstance[1].Play();
                        }
                    }
                        msmainChar.Sprite.MoveBy(5, 0);
                        velocity.X = 4f;
                     mDirection.X = MOVE_RIGHT;
                    break;

                case (int)CharState.Leftrun:
                    if (msmainChar.Sprite.CurrentAnimation != "runleft")
                    {
                        msmainChar.Sprite.CurrentAnimation = "runleft";
                    }
                    if (Col.PixelperPixelCollision(msmainChar, Levelobj[2].Texture, msmainChar.Position, Levelobj[2].Pos) != true)
                    {
                        if (Player.m_bSound[Player.currentProfile] == true)
                        {
                            if (soundEffectInstance[1].State == SoundState.Stopped)
                            {
                                soundEffectInstance[1].Play();
                            }
                        }
                        msmainChar.Sprite.MoveBy(-5, 0);
                        velocity.X = -4f;
                    }
                    else
                    {
                        velocity.X = 0f;
                    }
                    mDirection.X = MOVE_LEFT;
                    break;

                case (int)CharState.Leftwalk:
                    if (msmainChar.Sprite.CurrentAnimation != "walkleft")
                    {
                        msmainChar.Sprite.CurrentAnimation = "walkleft";
                    }
                    if (Col.PixelperPixelCollision(msmainChar, Levelobj[2].Texture, msmainChar.Position, Levelobj[2].Pos) != true)
                    {
                        if (Player.m_bSound[Player.currentProfile] == true)
                        {
                            if (soundEffectInstance[1].State == SoundState.Stopped)
                            {
                                soundEffectInstance[1].Play();
                            }
                        }
                        msmainChar.Sprite.MoveBy(-2, 0);
                        velocity.X = -3f;
                    }
                    else
                    {
                        velocity.X = 0f;
                    }
                    mDirection.X = MOVE_LEFT;
                    break;

                case (int)CharState.Rightwalk:
                    if (msmainChar.Sprite.CurrentAnimation != "walkright")
                    {
                        msmainChar.Sprite.CurrentAnimation = "walkright";
                    }
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                        if (soundEffectInstance[1].State == SoundState.Stopped)
                        {
                            soundEffectInstance[1].Play();
                        }
                    }
                    msmainChar.Sprite.MoveBy(+2, 0);
                    velocity.X = 3f;
                    mDirection.X = MOVE_RIGHT;
                    break;

                default:
                    if (msmainChar.Sprite.CurrentAnimation != "idle")
                    {
                        msmainChar.Sprite.CurrentAnimation = "idle";
                    }
                    if (soundEffectInstance[1].State != SoundState.Stopped)
                    {
                        soundEffectInstance[1].Stop();
                    }
                    velocity.X = 0f;
                    break;
            }

            for (int i = 0; i < platformpos.Length; i++ )
            {
                Levelobj[5].Pos = platformpos[i];
                if (Col.IsOnTopOf(Player.msmainChar.BoundingBox, Levelobj[5].BoundingRectangle) == true)
                {
                    JState = (int)JumpState.nJumping;
                    onPlatform = true;
                    gravity = false;
                }
            }

            if ((msmainChar.Sprite.Position.X >= GroundPos.Y || msmainChar.Sprite.Position.X <= GroundPos.X) && onPlatform == false)
            {
                gravity = true;
            }

            if (msmainChar.Position.Y < 620 && onPlatform == false)
                gravity = true;
            if (gravity == true)
            {
                if ((msmainChar.Sprite.Position.X <= GroundPos.Y && msmainChar.Sprite.Position.X >= GroundPos.X) && msmainChar.Position.Y >= 620)
                {
                    msmainChar.Position = new Vector2(msmainChar.Position.X, 620);
                    gravity = false;
                }
                else
                {
                    msmainChar.Sprite.MoveBy(0, 6);
                }
            }
            if (msmainChar.Sprite.Position.Y >= 768)
            {
                isAlive = false;
                gravity = false;
            }
            onPlatform = false;
        }
    }
}
