﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace LuluXNA
{
    class CLevel0
    {
        enum GameState { SplashScreen = 1, Menu, InGameMenu, Playing, Paused, LoadGame, Options, Credits, End, Profile, Lv2, Save, Paused1,  };
        enum Lv0State { Start, Car, End};
        enum CharState { Idle = 0, Leftwalk, Rightwalk, Rightrun, Leftrun, Rightjump, Leftjump, JumpIdle };
        public Texture2D[] Lv0Tex = new Texture2D[2];
        public CGameObject[] Lv0Obj = new CGameObject[3];
        private Vector2[] parallax = new Vector2[7];
        public Vector2 velocity = new Vector2(0, 0);
        private CCollision Col = new CCollision();
        private bool init = false;
        public int LState = (int)Lv0State.Start;
        public bool fadein = false;
        public float alpha = 0f;
        private SoundEffect[] effect = new SoundEffect[4];
        SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[4];

        public CLevel0()
        {
            parallax[0] = new Vector2(0, 0);
            parallax[1] = new Vector2(0.3f);
            parallax[2] = new Vector2(0.5f);
            parallax[3] = new Vector2(0.8f);
            parallax[4] = new Vector2(1f);
            parallax[5] = new Vector2(1.3f);
            parallax[6] = new Vector2(1.5f);
            Lv0Obj[0] = new CGameObject();
            Lv0Obj[1] = new CGameObject();
            Lv0Obj[2] = new CGameObject();
            Lv0Obj[0].Pos = new Vector2(0, 450);
            Lv0Obj[1].Pos = new Vector2(789, 520);
            Lv0Obj[2].Pos = new Vector2(690, 490);
        }

       public void LoadContent(ContentManager myContent)
       {
               Lv0Tex[0] = myContent.Load<Texture2D>("Level0/Lv0_bg");
               Lv0Tex[1] = myContent.Load<Texture2D>("Level0/Lv0_gras");
               Lv0Obj[0].Texture = myContent.Load<Texture2D>("Level0/Lv0_bush");
               Lv0Obj[1].Texture = myContent.Load<Texture2D>("Level0/Lv0_car");
               Lv0Obj[2].Texture = myContent.Load<Texture2D>("Level0/red");
               effect[0] = myContent.Load<SoundEffect>("Audio/nightforest");
               soundEffectInstance[0] = effect[0].CreateInstance();
               soundEffectInstance[0].IsLooped = true;
               effect[1] = myContent.Load<SoundEffect>("Audio/car");
               soundEffectInstance[1] = effect[1].CreateInstance();
               soundEffectInstance[1].IsLooped = false;
               effect[2] = myContent.Load<SoundEffect>("Audio/meow");
               soundEffectInstance[2] = effect[2].CreateInstance();
               soundEffectInstance[2].IsLooped = false;
               effect[3] = myContent.Load<SoundEffect>("Audio/walk");
               soundEffectInstance[3] = effect[3].CreateInstance();
               soundEffectInstance[3].IsLooped = false;
       }

       public void Initialize()
       {
           Lv0Obj[1].Pos = new Vector2(789, 520);
           init = false;
           fadein = false;
           alpha = 0f;
       }

       public int UpdateLevel0(CPlayerdata Player, CControls Controller, int GState, GameTime gameTime, CCamera2D cam, GraphicsDevice graphics)
       {
           if (Player.m_bSound[Player.currentProfile] == true)
           {
              soundEffectInstance[0].Play(); 
           } 
           if (init == false)
           {
               Player.msmainChar.Position = new Vector2(200,650);
               init = true;
           }
           GState = Controller.GetInput(GState, gameTime, Player.gravity, Player.Lv);
           Player.PoldPos = Player.msmainChar.Sprite.Position;
           Player.msmainChar.Update(gameTime);
           MoveMainChar(Controller.CState, gameTime, Player, Col, Lv0Obj);
           // Updates your camera to lock on the character
           cam.Update(cam, Player);

           if (Col.Intersects(Player.msmainChar.Sprite.BoundingBox, Lv0Obj[1].BoundingRectangle))
           {
               if (Player.m_bSound[Player.currentProfile] == true)
               {
                   soundEffectInstance[1].Play();
                   soundEffectInstance[2].Play();
               }                
               LState = (int)Lv0State.Car;
           }
           if (LState == (int)Lv0State.Car)
           {
               Lv0Obj[1].Pos.X += (5 + velocity.X);
               Lv0Obj[2].Pos.X = Lv0Obj[1].Pos.X - 99;
               if (Lv0Obj[1].Pos.X > 1500)
               {
                   LState = (int)Lv0State.End;
                   alpha = 0f;
               }
           }
           if (LState == (int)Lv0State.End && Player.msmainChar.Position.X > 1500)
           {
               soundEffectInstance[0].Stop();
               soundEffectInstance[1].Stop();
                   if (fadein == true)
                   {
                       GState = (int)GameState.Playing;
                       Player.msmainChar.Position = new Vector2(200, 620);
                   }
           }

           return GState;
       }

       public int DrawLevel0(SpriteBatch spriteBatch, CCamera2D cam, CPlayerdata Player, CMenu MMenu, int GState, CControls Controller, GameTime gameTime)
       {
           spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
           //Background 
           spriteBatch.Draw(Lv0Tex[0], new Vector2(0, 0), Color.White);
           //Bush
           spriteBatch.Draw(Lv0Obj[0].Texture, Lv0Obj[0].Pos, Color.White);
           //Car
           spriteBatch.Draw(Lv0Obj[1].Texture, Lv0Obj[1].Pos, Color.White);
           spriteBatch.End();
           //draw light
           spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive, null, null, null, null, cam.GetViewMatrix(parallax[4]));
                spriteBatch.Draw(Lv0Obj[2].Texture, Lv0Obj[2].Pos, Color.White);
           spriteBatch.End();

           spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
           //draw Player
           Player.msmainChar.Draw(spriteBatch);

           //Grass
           spriteBatch.Draw(Lv0Tex[1], new Vector2(0, 690), Color.White);
           if (LState == (int)Lv0State.End)
           {
               if (Player.msmainChar.Position.X > 1500 && fadein != true)
               {
                   if (alpha >= 1f)
                   {
                       fadein = true;
                   }
                   alpha = Controller.Fadein(spriteBatch, cam.Pos, alpha);
               }
           }

           spriteBatch.End();

           return GState;
       }

       public void MoveMainChar(int CState, GameTime gameTime, CPlayerdata Player, CCollision Col, CGameObject[] Levelobj)
       {
           Player.msmainChar.Position += velocity;
           switch (CState)
           {
               case (int)CharState.Leftwalk:
                   if (Player.msmainChar.Sprite.CurrentAnimation != "walkleft")
                    {
                        Player.msmainChar.Sprite.CurrentAnimation = "walkleft";
                    }
                   if (Col.PixelperPixelCollision(Player.msmainChar, Levelobj[0].Texture, Player.msmainChar.Position, Levelobj[0].Pos) != true)
                    {
                        if (Player.m_bSound[Player.currentProfile] == true)
                        {
                            soundEffectInstance[3].Play();
                        } 
                       velocity.X = -3f;
                       soundEffectInstance[3].Play();
                    }
                    else
                    {
                        velocity.X = 0f;
                    }
                    break;

                case (int)CharState.Rightwalk:
                    if (Player.msmainChar.Sprite.CurrentAnimation != "walkright")
                    {
                        Player.msmainChar.Sprite.CurrentAnimation = "walkright";
                    }
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                       soundEffectInstance[3].Play();
                    }
                    Player.msmainChar.Sprite.MoveBy(+2, 0);
                    velocity.X = 3f;
                    break;

                default:
                    if (Player.msmainChar.Sprite.CurrentAnimation != "idle")
                    {
                        Player.msmainChar.Sprite.CurrentAnimation = "idle";
                    }
                    velocity.X = 0f;
                    break;
            }
           if (velocity.X == 0f)
           {
               soundEffectInstance[3].Stop();
           }

       }

    }
}
