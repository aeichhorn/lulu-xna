﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using System.Timers;
using System.Runtime.InteropServices;

namespace LuluXNA
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Statemachine : Microsoft.Xna.Framework.Game
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern uint MessageBox(IntPtr hWnd, String text, String caption, uint type);
        GraphicsDeviceManager graphics;
        ContentManager myContent;
        CCamera2D cam;
        SpriteBatch spriteBatch;
        
        enum GameState { SplashScreen = 1, Menu, InGameMenu, Playing, Paused, LoadGame, Options, Credits, End, Profile, Lv2, Save, Paused1, Lv0 };
        enum Lv1State { Start, Loading, Ravenin, Ravenout, Key, Gateopen, Playing };
        enum Lv2State { Start, Loading, Switchon, Switchoff, Spider, End, BPressed };
        enum Lv0State { Start, Car, End };
        private int GState = (int)GameState.SplashScreen;
        CControls Controller = new CControls();
        CMenu MMenu = new CMenu();
        private float elapsed = 0;
        private bool lmt = false;
        CPlayerdata Player;
        CLevel1 Lv1 = new CLevel1();
        CLevel2 Lv2 = new CLevel2();
        CLevel0 Lv0 = new CLevel0();

        public Statemachine()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            myContent = new ContentManager(this.Services); 
            myContent.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            MMenu.MenuChoice = (int)GameState.Playing;
            MMenu.MenuState = (int)GameState.Playing;
            cam = new CCamera2D(graphics.GraphicsDevice.Viewport);
            Controller.m_mousePos = Vector2.Zero;
            Controller.gamePadState = GamePad.GetState(PlayerIndex.One);
            Controller.keyboardState = Keyboard.GetState();
            Controller.mouseState = Mouse.GetState();
            Player = new CPlayerdata(graphics, spriteBatch);
            cam.Limits = new Rectangle(0, 0, 3072, 768);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Hier laden wir unsere Grafik und weisen sie unserem Texture2D-Objekt zu
            MMenu.LoadContent(myContent); //Menüpics laden
            Player.LoadPlayerContent(myContent, graphics.GraphicsDevice.Viewport);
            Player.LoadProfileContent(myContent);
            Lv1.LoadContent(myContent);
            Lv2.LoadContent(myContent);
            Lv0.LoadContent(myContent);
            Controller.LoadContent(myContent);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Controller.gamePadState = GamePad.GetState(PlayerIndex.One);
            Controller.keyboardState = Keyboard.GetState();
            Controller.mouseState = Mouse.GetState();
            elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
           try
            {
                if (GState == (int)GameState.SplashScreen && ((Controller.kboldState.IsKeyUp(Keys.Enter) && Controller.keyboardState.IsKeyDown(Keys.Enter)) ||
                    (Controller.mouseState.LeftButton == ButtonState.Pressed && Controller.moldState.LeftButton == ButtonState.Released) ||
                    (Controller.gamePadState.Buttons.A == ButtonState.Pressed && Controller.gpoldState.Buttons.A == ButtonState.Released)))
                {
                    GState = (int)GameState.Menu;
                    MMenu.MenuState = 0;
                }
                else if (GState == (int)GameState.Menu)
                {
                    GState = MMenu.UpdateMenu(Player, Controller, GState, spriteBatch);
                    if (Player.m_bSound[Player.currentProfile] == true )
                    {
                        if (MMenu.soundEffectInstance[0].State == SoundState.Stopped)
                        {
                            MMenu.soundEffectInstance[0].Play();
                        }
                    }
                }
                else if (GState == (int)GameState.Profile)
                {
                    GState = Player.UpdateProfileMenu(MMenu, Controller, spriteBatch, GState, Lv1, ref Player, ref Lv2, ref Lv0);
                }
                else if (GState == (int)GameState.Credits)
                {
                    MMenu.MenuState = Controller.MenuControl(MMenu.MenuState, 6);
                    GState = MMenu.ChooseMenu(Controller, spriteBatch, GState);
                }
                else if (GState == (int)GameState.Options)
                {
                    GState = MMenu.UpdateOptions(Player, spriteBatch, Controller, GState, graphics);
                }
                else if (GState == (int)GameState.Lv0)
                {
                    if (lmt == false)
                    {
                        cam.Limits = new Rectangle(0, 0, 1500, 768);
                        lmt = true;
                    }
                    GState = Lv0.UpdateLevel0(Player, Controller, GState, gameTime, cam, graphics.GraphicsDevice);
                }
                else if ((GState == (int)GameState.Playing || GState == (int)GameState.Paused) && Player.Lv == 1)
                {
                    if (lmt == true)
                    {
                        cam.Limits = new Rectangle(0, 0, 3072, 768);
                        lmt = false;
                    }
                    GState = Lv1.UpdateLevel1(Player, Controller, GState, gameTime, cam, graphics.GraphicsDevice, Lv1.LState, ref Lv2);
                }
                else if (GState == (int)GameState.LoadGame)
                {
                        GState = Player.loadGame(ref GState, ref Lv1.LState, ref Lv2.LState, ref Lv1.Raven, ref Lv1.Speaking, ref Lv1.gate, ref Lv1.Levelobj[8], ref Lv1.didShowMsgB, Lv1.msgcount,
                           ref Lv1.RavenMsg, ref Lv1.MymsgBox, ref Lv2.save, ref Lv2.Levelobj, ref Lv2.didShowMsgB[0], ref Lv2.MymsgBox.Count);
                }
                else if ((GState == (int)GameState.Lv2 || GState == (int)GameState.Paused1) && Player.Lv == 2)
                {
                    Player.Lv = 2;
                    GState = Lv2.UpdateLevel2(Player, Controller, GState, gameTime, cam, graphics.GraphicsDevice, Lv2.LState, ref Lv1, ref MMenu);
                    if (Lv2.LState == (int)Lv2State.BPressed)
                    {
                        Lv2.LState = Lv2.InitializeLv2(ref Player);
                        Lv1.LState = Lv1.InitializeLv1(Player, Lv1.MymsgBox, Controller);
                        Lv0.Initialize();
                        Lv0.LState = (int)Lv0State.Start;
                        Player.Lv = 1;
                    }
                }
                else if (GState == (int)GameState.Save)
                {
                     if (Player.Lv == 1)
                     {
                       GState = Player.saveGame(Lv1.LState, Lv2.LState, (int)GameState.Playing, (int)Lv1.Levelobj[8].Pos.X, Lv1.MymsgBox.Count, Lv1.Raven, Lv1.msgcount, Lv1.didShowMsgB, Lv1.Speaking, Lv1.gate, ref Lv1.RavenMsg.show,
                            ref Lv2.save, ref Lv2.Levelobj, ref Lv2.didShowMsgB[0],ref Lv2.MymsgBox.Count);
                     }
                     else
                     {
                        GState = Player.saveGame(Lv1.LState, Lv2.LState, (int)GameState.Lv2, (int)Lv1.Levelobj[8].Pos.X, Lv1.MymsgBox.Count, Lv1.Raven, Lv1.msgcount, Lv1.didShowMsgB, Lv1.Speaking, Lv1.gate, ref Lv1.RavenMsg.show,
                             ref Lv2.save, ref Lv2.Levelobj, ref Lv2.didShowMsgB[0], ref Lv2.MymsgBox.Count);
                     }
                }
                if (MMenu.soundEffectInstance[0].State != SoundState.Stopped && (GState == (int)GameState.Lv2 || GState == (int)GameState.Paused1 || GState == (int)GameState.Lv0 || GState == (int)GameState.Playing || GState == (int)GameState.Paused))
                {
                    MMenu.soundEffectInstance[0].Stop();
                }
            }
            catch (System.ArgumentNullException)
            {
                GState = (int)GameState.Menu;
                MessageBox(new IntPtr(0), "An Error occured!", "Null Reference Exception", 0);
            }
           catch (System.IndexOutOfRangeException)
            {
                GState = (int)GameState.Menu;
                MessageBox(new IntPtr(0), "An Error occured!", "Index out of Range Exception", 0);
            }
            
            Controller.m_mousePos.X = Controller.mouseState.X;
            Controller.m_mousePos.Y = Controller.mouseState.Y;
            // If the exit key is pressed, skip the rest of the update.
            if (Controller.exitKeyPressed() == false)
            {
                //Check if player wants to end the game
                if (Controller.endPressed(Controller, MMenu.MenuChoice, (int)GameState.End) == true)
                {
                    Exit();
                }
                base.Update(gameTime);
            }
            else
            {
                Exit();
            }

            Controller.kboldState = Controller.keyboardState;
            Controller.moldState = Controller.mouseState;
            Controller.gpoldState = Controller.gamePadState;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //Draw the right sprites depending on gamestate
            if (GState == (int)GameState.SplashScreen)
            {
                MMenu.DrawSplashScreen(spriteBatch);
            }
            else if (GState == (int)GameState.Menu)
            {
                MMenu.DrawMenu(spriteBatch, Player, Controller);
            }
             else if (GState == (int)GameState.Profile)
             {
                 Player.DrawProfileMenu(spriteBatch, Player, Controller, MMenu, gameTime);
             }
            else if (GState == (int)GameState.Credits)
            {
                MMenu.DrawCredits(spriteBatch, Controller);
            }
            else if (GState == (int)GameState.Options)
            {
                MMenu.DrawOptions(spriteBatch, Controller, Player);
            }
            else if (GState == (int)GameState.Lv0)
            {
                GState = Lv0.DrawLevel0(spriteBatch, cam, Player, MMenu, GState, Controller, gameTime);
            }
            else if (GState == (int)GameState.Playing || GState == (int)GameState.Paused)
            {
                GState = Lv1.DrawLevel1(spriteBatch, cam, Player, graphics.GraphicsDevice, MMenu, GState, Controller, gameTime, ref Lv2);
            }
            else if (GState == (int)GameState.Lv2 || GState == (int)GameState.Paused1)
            {
                GState = Lv2.DrawLevel2(spriteBatch, cam, Player, MMenu, GState, Controller);
            }

            // draw cursor
            spriteBatch.Begin();
            spriteBatch.Draw(MMenu.m_mouseTexture, Controller.m_mousePos, null, Color.White, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0f);
            spriteBatch.End();
	
            base.Draw(gameTime);
        }

    }
}

