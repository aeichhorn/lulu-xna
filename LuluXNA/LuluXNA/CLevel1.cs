﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace LuluXNA
{
    class CLevel1
    {
        public CGameObject[] Levelobj = new CGameObject[15];
        public Texture2D[] DayNight = new Texture2D[9];
        private Vector2[] GroundPlan = new Vector2[1];
        enum GameState { SplashScreen = 1, Menu, InGameMenu, Playing, Paused, LoadGame, Options, Credits, End, Profile, Lv2, Save, Paused1 };
        enum Lv1State { Start, Loading, Ravenin, Ravenout, Key, GotKey, Gateopen, Playing };
        private Vector2[] parallax = new Vector2[7];
        private Texture2D sparkle;
        private CMobileSprites Sparkle;
        bool checkp = false;
        public bool fadeout = false;
        public bool fadein = false;
        public float alpha = 1f;

        private const int MSGCOUNT = 3; //Amount of MessageBoxes in the Level
        public int msgcount = MSGCOUNT;
        public CMsBox MymsgBox = new CMsBox();
        public CMsBox RavenMsg = new CMsBox();
        private int[] MBposX = new int[MSGCOUNT];
        public bool[] didShowMsgB = new bool[MSGCOUNT];
        string[] Messages = new string[2];
        public bool Speaking = false;
        public bool gate = false;

        private Texture2D raven;
        public CMobileSprites Raven;
        public int LState = (int)Lv1State.Start;
        private CCollision Col;
        private float _elapsedSeconds;
        private float _elapsedSeconds1;
        private float _elapsedSeconds2;
        private double time;
        private int[] Order = new int[10];
        private int curNr = 0;
        private bool rain = false;
        private bool rainani = false;
        private SoundEffect[] effect = new SoundEffect[6];
        SoundEffectInstance[] soundEffectInstance = new SoundEffectInstance[6];

        public CLevel1()
        {
            for (int i = 0; i < Levelobj.Length; i++ )
            {
                Levelobj[i] = new CGameObject();
            }
            parallax[0] = new Vector2(0,0);
            parallax[1] = new Vector2(0.3f);
            parallax[2] = new Vector2(0.5f);
            parallax[3] = new Vector2(0.8f);
            parallax[4] = new Vector2(1f);
            parallax[5] = new Vector2(1.3f);
            parallax[6] = new Vector2(1.5f);
            GroundPlan[0] = new Vector2(0, 3072);
            MBposX[0] = 200;
            MBposX[1] = 2500;
            MBposX[2] = 1488;
            Col = new CCollision();
            Levelobj[6].Pos = new Vector2(2860, 200);
            Levelobj[8].Pos = new Vector2(672, 560);
            Levelobj[9].Pos = new Vector2(672, 560);
            Levelobj[7].Pos = new Vector2(330, 341);
            Levelobj[9].Pos = new Vector2(200, 355);
            Levelobj[10].Pos = new Vector2(Levelobj[8].Pos.X + 50, Levelobj[8].Pos.Y +50);
            Levelobj[13].Pos = new Vector2(135, 291);
            Levelobj[12].Pos = new Vector2(1472, 300);
            Levelobj[14].Pos = new Vector2(320, 321);
            Order[0] = 2;
            Order[1] = 1;
            Order[2] = 0;
            Order[3] = 0;
            Order[4] = 1;
            Order[5] = 2;
            Order[6] = 5;
            Order[7] = 4;
            Order[8] = 4;
            Order[9] = 5;

        }

        public void LoadContent(ContentManager myContent)
        {
            Levelobj[0].Texture = myContent.Load<Texture2D>("Level1/level1_e3");
            Levelobj[1].Texture = myContent.Load<Texture2D>("Level1/level1_fog");
            Levelobj[2].Texture = myContent.Load<Texture2D>("Level1/level1_oe");
            Levelobj[3].Texture = myContent.Load<Texture2D>("Level1/level1_e21");
            Levelobj[4].Texture = myContent.Load<Texture2D>("Level1/level1_e2");
            Levelobj[5].Texture = myContent.Load<Texture2D>("Level1/baum_lv1");
            Levelobj[6].Texture = myContent.Load<Texture2D>("Level1/gate_lv1");
            Levelobj[7].Texture = myContent.Load<Texture2D>("Level1/key_lv1");
            Levelobj[8].Texture = myContent.Load<Texture2D>("Level1/busch_lv1");
            Levelobj[9].Texture = myContent.Load<Texture2D>("Level1/platform_lv1");
            Levelobj[10].Texture = Levelobj[9].Texture;
            Levelobj[11].Texture = myContent.Load<Texture2D>("Level1/PressA");
            Levelobj[12].Texture = myContent.Load<Texture2D>("Level1/PressB");
            Levelobj[13].Texture = myContent.Load<Texture2D>("Level1/baum_ast_lv1");
            Levelobj[14].Texture = myContent.Load<Texture2D>("Level1/checkpoint");
            DayNight[0] = myContent.Load<Texture2D>("DNCycle/day_1");
            DayNight[1] = myContent.Load<Texture2D>("DNCycle/day_2");
            DayNight[2] = myContent.Load<Texture2D>("DNCycle/day_3");
            DayNight[3] = myContent.Load<Texture2D>("DNCycle/night");
            DayNight[4] = myContent.Load<Texture2D>("DNCycle/night_trans");
            DayNight[5] = myContent.Load<Texture2D>("DNCycle/night1");
            DayNight[6] = myContent.Load<Texture2D>("DNCycle/dawn");
            DayNight[7] = myContent.Load<Texture2D>("DNCycle/rain1");
            DayNight[8] = myContent.Load<Texture2D>("DNCycle/rain2");
            MymsgBox.LoadContent(myContent);
            RavenMsg.LoadContent(myContent);
            sparkle = myContent.Load<Texture2D>(@"Level1\sparke_lv1");
            Sparkle = new CMobileSprites(sparkle);
            Sparkle.Sprite.AddAnimation("sparkle", 0, 0, 107, 50, 2, 0.4f);
            Sparkle.Sprite.CurrentAnimation = "sparkle";
            Sparkle.Position = new Vector2(332, 328);
            Sparkle.Sprite.AutoRotate = false;
            Sparkle.Speed = 3;
            Sparkle.LoopPath = false;
            Sparkle.IsPathing = false;
            Sparkle.IsMoving = false;
            raven = myContent.Load<Texture2D>(@"NPC\crow_lv1");
            Raven = new CMobileSprites(raven);
            Raven.Sprite.AddAnimation("sitting", 0, 0, 96, 118, 1, 0.1f);
            Raven.Sprite.AddAnimation("flying", 96, 0, 148, 177, 9, 0.2f);
            Raven.Sprite.CurrentAnimation = "flying";
            Raven.Position = new Vector2(1000, -180);
            Raven.Sprite.AutoRotate = false;
            Raven.Target = new Vector2(1472, 400);
            Raven.Speed = 3;
            Raven.LoopPath = false;
            Raven.IsPathing = false;
            Raven.IsMoving = false;
            effect[0] = myContent.Load<SoundEffect>("Audio/rain");
            soundEffectInstance[0] = effect[0].CreateInstance();
            soundEffectInstance[0].IsLooped = false;
            effect[1] = myContent.Load<SoundEffect>("Audio/crow");
            soundEffectInstance[1] = effect[1].CreateInstance();
            soundEffectInstance[1].IsLooped = false;
            effect[2] = myContent.Load<SoundEffect>("Audio/gate");
            soundEffectInstance[2] = effect[2].CreateInstance();
            soundEffectInstance[2].IsLooped = false;
            effect[3] = myContent.Load<SoundEffect>("Audio/nightforest");
            soundEffectInstance[3] = effect[3].CreateInstance();
            soundEffectInstance[3].IsLooped = false;
            effect[4] = myContent.Load<SoundEffect>("Audio/forestbirds");
            soundEffectInstance[4] = effect[4].CreateInstance();
            soundEffectInstance[4].IsLooped = true;
            effect[5] = myContent.Load<SoundEffect>("Audio/meow");
            soundEffectInstance[5] = effect[5].CreateInstance();
            soundEffectInstance[5].IsLooped = false;

        }

        public int InitializeLv1(CPlayerdata Player, CMsBox MBox, CControls Controller){
            Levelobj[8].Pos = new Vector2(672, 560);
            Player.isAlive = true;
            Player.msmainChar.Sprite.CurrentAnimation = "idle";
            Player.velocity = Vector2.Zero;
            Player.gravity = false;
            Player.mDirection = Vector2.Zero;
            MBox.Count = 0;
            Raven.Position = new Vector2(1000, -180);
            Raven.Target = new Vector2(1472, 400);
            Raven.IsMoving = false;
            Raven.Sprite.CurrentAnimation = "flying";
            Player.PoldPos = new Vector2(200, 620);
            Player.msmainChar.Position = new Vector2(200, 620);
            for (int i = 0; i < MSGCOUNT; i++ ){
                didShowMsgB[i] = false;
            }
            Speaking = false;
            gate = false;
            Controller.PKeyPressed = false;
            Controller.MState = 0;
            Controller.CState = 0;
            Controller.JState = 2;
            RavenMsg.show = false;
            fadeout = false;
            alpha = 1f;
            fadein = false;
            return (int)Lv1State.Playing;
        }

        public int UpdateLevel1(CPlayerdata Player, CControls Controller, int GState, GameTime gameTime, CCamera2D cam, GraphicsDevice graphics, int LvState, ref CLevel2 Lv2)
        {
            try
            {
               if (Player.m_bSound[Player.currentProfile] == true)
               {
                   if (soundEffectInstance[4].State == SoundState.Stopped)
                   {
                     soundEffectInstance[4].Play();
                   }
               }
                     LState = LvState;
                     time = gameTime.ElapsedGameTime.TotalSeconds;
                     _elapsedSeconds += (float)gameTime.ElapsedGameTime.TotalSeconds;
                     _elapsedSeconds1 += (float)gameTime.ElapsedGameTime.TotalSeconds;

                     if (_elapsedSeconds - time >= 20)
                     {
                         curNr = (curNr + 1)%10;
                         _elapsedSeconds = 0;
                     } 
                     if (_elapsedSeconds1 - time >= 50)
                     {
                         if(rain == false)
                            rain = true;
                         else if(rain == true)
                            rain = false;
                         _elapsedSeconds1 = 0;
                     }
                    if (LState == (int)Lv1State.Start)
                     {
                         LState = InitializeLv1(Player, MymsgBox, Controller);
                     }
                     GState = Controller.GetInput(GState, gameTime, Player.gravity, Player.Lv);
                     if (GState != (int)GameState.Paused)
                     {
                            Player.PoldPos = Player.msmainChar.Sprite.Position;
                            MymsgBox.UpdateMsgBox(Controller, cam, ref didShowMsgB);
                            Player.msmainChar.Update(gameTime);
                            if (MymsgBox.showMsgB != true)
                            {
                                if (LState == (int)Lv1State.Key)
                                {
                                    Sparkle.Update(gameTime);
                                    if (Player.msmainChar.Position.X >= 1452 && Player.msmainChar.Position.X <= 1500)
                                    {
                                        if (Controller.BPressed() == true)
                                        {
                                            if (Speaking == false)
                                            {
                                                if (Player.m_bSound[Player.currentProfile] == true)
                                                {
                                                    if (soundEffectInstance[5].State == SoundState.Stopped)
                                                    {
                                                        soundEffectInstance[5].Play();
                                                    }
                                                }
                                                Speaking = true;                                        
                                            } else {
                                                Speaking = false;
                                            }
                                            
                                        }
                                        Speaking = RavenMsg.UpdateMsgBox(Controller, cam, Speaking, gameTime);
                                    }
                                }
                                Raven.Update(gameTime);
                                if (LState == (int)Lv1State.Ravenin)
                                {
                                    if (Player.m_bSound[Player.currentProfile] == true)
                                    {
                                        if (soundEffectInstance[1].State == SoundState.Stopped)
                                        {
                                            soundEffectInstance[1].Play();
                                        }
                                    }
                                    Raven.IsMoving = true;
                                    if (Raven.Target == Raven.Position)
                                    {
                                        Raven.Sprite.CurrentAnimation = "sitting";
                                        LState = (int)Lv1State.Key;
                                    }
                                }
                                else if (LState == (int)Lv1State.Key)
                                {
                                    if (Col.Intersects(Player.msmainChar.BoundingBox, Levelobj[7].BoundingRectangle))
                                    {
                                        LState = (int)Lv1State.GotKey;
                                    }
                                }
                                else if (LState == (int)Lv1State.GotKey)
                                {
                                    if (Player.msmainChar.Position.X >= 1452 && Player.msmainChar.Position.X <= 1500)
                                    {
                                        if (Controller.BPressed() == true)
                                        {
                                            if (Speaking == false)
                                            {
                                                Speaking = true;
                                            }
                                            else
                                            {
                                                Speaking = false;
                                                LState = (int)Lv1State.Ravenout;
                                            }
                                        }
                                        Speaking = RavenMsg.UpdateMsgBox(Controller, cam, Speaking, gameTime);
                                    }
                                } 
                                else if (LState == (int)Lv1State.Ravenout)
                                {
                                    if (Player.m_bSound[Player.currentProfile] == true)
                                    {
                                        if (soundEffectInstance[1].State == SoundState.Stopped)
                                        {
                                            soundEffectInstance[1].Play();
                                        }
                                    }
                                    Raven.Sprite.CurrentAnimation = "flying";
                                    Raven.Target = new Vector2(3100, 300);
                                    LState = (int)Lv1State.Gateopen;
                                } else if (LState == (int)Lv1State.Gateopen)
                                {
                                    if (Raven.Position == Raven.Target)
                                    {
                                        if (Player.m_bSound[Player.currentProfile] == true)
                                        {
                                            if (soundEffectInstance[2].State == SoundState.Stopped)
                                            {
                                                soundEffectInstance[2].Play();
                                            }
                                        }
                                        gate = true;
                                    }
                                    if (Raven.Position == Raven.Target && Player.msmainChar.Position.X >=3000 && fadein == true)
                                    {
                                            soundEffectInstance[0].Stop();
                                            soundEffectInstance[4].Stop();
                                            soundEffectInstance[3].Stop();
                                            GState = (int)GameState.Lv2;
                                            LState = (int)Lv1State.Start;
                                            Player.Lv = 2;
                                    }
                                }
                                Player.UpdateMoveChar(Controller.CState, Controller.JState, gameTime, GroundPlan, cam, Player, Col, Levelobj, gate);
                            }
                            if (MymsgBox.Count != MSGCOUNT)
                            {
                                if (Player.msmainChar.Sprite.Position.X >= MBposX[MymsgBox.Count] && Player.msmainChar.Sprite.Position.X <= MBposX[MymsgBox.Count] + 50 && 
                                    MymsgBox.Count < MBposX.Length && didShowMsgB[MymsgBox.Count] == false)
                                {
                                       MymsgBox.showMsgB = true;
                                }
                            }
                            // Updates your camera to lock on the character
                            cam.Update(cam, Player);
                     }
                     else
                     {
                           GState = Controller.UpdateIngameMenu(GState, graphics, cam.Pos,ref LState);
                     }
            }
            catch (System.IndexOutOfRangeException)
            {
                LState = (int)Lv1State.Start;
                return (int)GameState.Menu;
            }
            return GState;
        }

        public int DrawLevel1(SpriteBatch spriteBatch, CCamera2D cam, CPlayerdata Player, GraphicsDevice graphics, CMenu MMenu, int GState, CControls Controller, GameTime gameTime, ref CLevel2 Lv2)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[1]));
                //Heaven
                if (Order[curNr] == 5 || Order[curNr] == 4 && rain != true)
                {
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                        if (soundEffectInstance[3].State == SoundState.Stopped)
                        {
                            soundEffectInstance[3].Play();
                        }
                    }
                    if (soundEffectInstance[4].State != SoundState.Stopped)
                    {
                        soundEffectInstance[4].Stop();
                    }
                }
                else
                {
                    if (rain != true)
                    {
                        if (Player.m_bSound[Player.currentProfile] == true)
                        {
                            if (soundEffectInstance[4].State == SoundState.Stopped)
                            {
                                soundEffectInstance[4].Play();
                            }
                        }
                    }
                    soundEffectInstance[3].Stop();
                }
                spriteBatch.Draw(DayNight[Order[curNr]], Levelobj[0].Pos, Color.White);
                //Background Layer
                 spriteBatch.Draw(Levelobj[0].Texture, Levelobj[0].Pos, Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
                 //Grass and Stones
                 spriteBatch.Draw(Levelobj[4].Texture, Levelobj[4].Pos, Color.White);
                 //platform am Baum
                 spriteBatch.Draw(Levelobj[9].Texture, Levelobj[9].Pos, Color.White);                
                 //Baum links
                 spriteBatch.Draw(Levelobj[5].Texture, Levelobj[5].Pos, Color.White);
                 //Character Layer
                 Player.DrawCharacter();
                 Raven.Draw(spriteBatch);
                 if (LState == (int)Lv1State.Key)
                 {
                     spriteBatch.Draw(Levelobj[7].Texture, Levelobj[7].Pos, Color.White);
                 }
                 //Ast
                 spriteBatch.Draw(Levelobj[13].Texture, Levelobj[13].Pos, Color.White);
                 //platform am busch
                 Levelobj[10].Pos.X = Levelobj[8].Pos.X + 30;
                 Levelobj[10].Pos.Y= Levelobj[8].Pos.Y + 40;
                 spriteBatch.Draw(Levelobj[10].Texture, Levelobj[10].Pos, Color.White);
                 //Busch
                 spriteBatch.Draw(Levelobj[8].Texture, Levelobj[8].Pos, Color.White);
                 //Tor rechts
                 spriteBatch.Draw(Levelobj[6].Texture, Levelobj[6].Pos, Color.White);                 
                 //Cat Foreground Layer
                 spriteBatch.Draw(Levelobj[3].Texture, Levelobj[3].Pos, Color.White);
                 if (LState == (int)Lv1State.GotKey)
                 {  //draw Checkpoint
                     spriteBatch.Draw(Levelobj[14].Texture, Levelobj[14].Pos, Color.White);
                     if (checkp == false)
                     {
                         Player.saveGame(LState, Lv2.LState, (int)GameState.Playing, (int)Levelobj[8].Pos.X, MymsgBox.Count, Raven, msgcount, didShowMsgB, Speaking, gate, ref RavenMsg.show,
                             ref Lv2.save, ref Lv2.Levelobj, ref Lv2.didShowMsgB[0], ref Lv2.MymsgBox.Count);
                         if (Player.m_bSound[Player.currentProfile] == true)
                         {
                             if (soundEffectInstance[1].State == SoundState.Stopped)
                             {
                                 soundEffectInstance[1].Play();
                             }
                         }
                         checkp = true;
                     }
                 }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, null, null, null, cam.GetViewMatrix(parallax[3]));
                 //Fog Layer
            spriteBatch.Draw(Levelobj[1].Texture, Levelobj[1].Pos, Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[6]));
                 //Front Layer
            spriteBatch.Draw(Levelobj[2].Texture, Levelobj[2].Pos, Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.GetViewMatrix(parallax[4]));
            //show rain
            if (rain == true)
            {
                if (Player.m_bSound[Player.currentProfile] == true)
                {
                    if (soundEffectInstance[0].State == SoundState.Stopped)
                    {
                        soundEffectInstance[0].Play();
                    }
                }
                soundEffectInstance[4].Stop();
                _elapsedSeconds2 += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(rainani == true){
                    spriteBatch.Draw(DayNight[7], Levelobj[0].Pos, Color.White);
                }
                else {
                    spriteBatch.Draw(DayNight[8], Levelobj[0].Pos, Color.White);
                }
                if (_elapsedSeconds2 - time >= 0.5f)
                {
                    if (rainani == true) rainani = false;
                    else rainani = true;
                    _elapsedSeconds2 = 0;
                }
            }
            else
            {
                if (Order[curNr] != 5 || Order[curNr] != 4)
                {
                    if (Player.m_bSound[Player.currentProfile] == true)
                    {
                        if (soundEffectInstance[4].State == SoundState.Stopped)
                        {
                            soundEffectInstance[4].Play();
                        }
                    }
                }
                soundEffectInstance[0].Stop();
            }
            if (Order[curNr] == 3)
            {
                //Heaven
                spriteBatch.Draw(DayNight[4], Levelobj[0].Pos, Color.White);
            } else if (Order[curNr] == 2)
            {
                spriteBatch.Draw(DayNight[6], Levelobj[0].Pos, Color.White);
            }
            if (LState == (int)Lv1State.Key)
            {
                if (Player.msmainChar.Position.X >= 1452 && Player.msmainChar.Position.X <= 1500)
                {
                    if (Speaking == true)
                    {
                        Messages[0] = "Have you found it, yet?";
                        Messages[1] = "";
                        RavenMsg.DrawMsgBox(spriteBatch, cam, Messages, true, Controller, parallax[4]);
                    } 
                    spriteBatch.Draw(Levelobj[12].Texture, Levelobj[12].Pos, Color.White);
                }
                Sparkle.Draw(spriteBatch);
            }
            else if (LState == (int)Lv1State.GotKey)
            {
                if (Player.msmainChar.Position.X >= 1452 && Player.msmainChar.Position.X <= 1500)
                {
                    if (Speaking == true)
                    {
                        Messages[0] = "Great! Now I can open the gate for you! Wait!";
                        Messages[1] = "";
                        RavenMsg.DrawMsgBox(spriteBatch, cam, Messages, true, Controller, parallax[4]);
                    }
                    spriteBatch.Draw(Levelobj[12].Texture, Levelobj[12].Pos, Color.White);
                }
            }

            if (LState == (int)Lv1State.GotKey)
            {
                //Player has Key
                spriteBatch.Draw(Levelobj[7].Texture, cam.ScreenToWorld(new Vector2(50, 700), parallax[4]), Color.White);
            }
            if (GState == (int)GameState.Paused)
                {
                    MMenu.DrawIngameMenu(spriteBatch, Controller.MState, cam, MMenu, Player, parallax[4]);
                }
                if (MymsgBox.showMsgB == true)
                {
                    if (MymsgBox.Count == 0)
                        Messages[0] = "Where am I? What happened?"; 
                    else if (MymsgBox.Count == 1){
                        Messages[0] = "Hmm...It's locked!";
                        LState = (int)Lv1State.Ravenin;
                    }
                    else if (MymsgBox.Count == 2)
                    {
                        Messages[0] = "Find something sparkling and I am going ";
                        Messages[1] = "to open the gate for you!";
                    }
                     MymsgBox.DrawMsgBox(spriteBatch, cam, Messages, true, Controller, parallax[4]);
                }

                if (fadeout != true)
                {
                    if (alpha <= 0)
                    {
                        fadeout = true;
                    }
                    alpha = Controller.Fadeout(spriteBatch, cam.Pos, alpha);
                }
                if (Player.msmainChar.Position.X >= 3000)
                {
                    if (alpha >= 1)
                    {
                        fadein = true;
                    }
                    alpha = Controller.Fadein(spriteBatch, cam.Pos, alpha);
                }

            spriteBatch.End();
            return GState;
        }
    }
}
